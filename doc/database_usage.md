#### load sql file

```java
@Given("{string} is loaded into the database")
```

load the specified sql file in the resource folder using `ResourceDatabasePopulator`

BDD Example:

```yaml
Given "init_testdb.sql" is loaded into the database
```

#### save record using doc string and datatable

```java
@When("Table {string} has record")
@And("save table {string} with {string}")
@And("save table {string} as {string} with")
```
save the specified object provided by doc string into the named table. the " as {string}" is the newly saved entity 
variable name which accessible in SPel expression

BDD Example:

```yaml
When Table "TestEntity" has record
"""
id: id1
name: "name changed"
"""
And save table "TestEmbeddedEntity" with
| idVal | {idVal: 1, nameVal: id} |

When save table "TestEntity" as "TEST_ENTITY_VARIABLE" with
"""
name: test entity variable
"""
And query table "TestEntity" with
"""
name: test entity variable
"""
Then query results should equals to
"""
- "#{TEST_ENTITY_VARIABLE}"
"""
```

#### save record using expression

```java
@When("Table {string} has record {string}")
@And("save table {string} with {string}")
```

save the predefined variable into the named table.

BDD Example:

```yaml
Given define the following variables
"""
RECORD_1:
  id: id1
  name: name1
"""
And Table "TestEntity" has record "RECORD_1"
And save table "TestEmbeddedEntity" with "RECORD_1"
```

#### query table using docstring & datatable

```java
@When("query table/Table {string} with")
@When("query table {string} {timeoutSeconds}")
```

query named table with doc string with specified fields

BDD Example:

```yaml
When query Table "TestEntity" with
"""
id: id1
"""
When query table "TestEmbeddedEntity" with
"""
idVal.idVal: "1"
idVal.nameVal: ""
"""  
When query table "TestEmbeddedEntity" with
| idVal.idVal | "1" |

When query table "TestEmbeddedEntity" at 3 seconds timeout
"""
  idVal.idVal: "1"
  strValue:
"""
```

#### query result

this relies on register variable, "query results", for the latest query results. 

BDD Example:

```yaml
When query Table "TestEntity" with
"""
id: id1
"""
Then query results should equals to
"""
- id: id1
  name: "name changed"
"""

When query Table "TestEntity" with
"""
  id: id1
  name:
"""
Then query results should equals to
"""
  []
"""
And query results should equals to "{RECORD_1}"
```


#### assert table record

```java
@Then("Table {string} should{not} have record")  
```

check whether the table contains the similar record as defined in doc string

BDD Example:

```yaml
Then Table "TestEntity" should not have record
"""
id: id1
name: name1
"""
Then Table "TestEntity" should have record
"""
id: id1
name: "name changed"
"""
```

#### assert table record using expression

```java
@Then("Table {string} should{not} have record {string}")  
```

check whether the table contains the similar record as specified variable

BDD Example:

```yaml
And Table "TestEntity" should have record "RECORD_2"
And Table "TestEntity" should not have record "RECORD_1"
```
