#### submit request  

```java
@When("submit rest request")
```

submit a request defined in doc string. 
2 variables will be defined, "restRequest" & "restResponse" for request and received response  

BDD Example:

```yaml
When submit rest request
"""
  url: "http://localhost:#{REST_MOCK_WEB.localPort}"
  method: GET
"""
```

#### submit request using expression

```java
@When("submit rest request {string}")
```

submit a request using SpEL expression, such as predefined variable

BDD Example:

```yaml
And define the following variables
"""
RESTFUL_REQ:
  url: "http://localhost:#{REST_MOCK_WEB.localPort}"
  method: GET
"""
When submit rest request "RESTFUL_REQ"
```

#### submit request and label the request

```java
@When("submit rest request {string} as")
```

submit a request defined in doc string. the request will be defined as named variable to be used in expression

BDD Example:

```yaml
When submit rest request "RESTFUL_REQ_2" as
"""
url: "http://localhost:#{REST_MOCK_WEB.localPort}"
method: GET
"""
And the following checks should pass
| RESTFUL_REQ_2.url == RESTFUL_REQ.url            |
| RESTFUL_REQ_2.method.name == RESTFUL_REQ.method |
```

#### label received response 

```java
@And("rest response is stored as {string}")
```

last received response will be stored as named variable. 

BDD Example:

```yaml
And rest response is stored as "RECEIVED_RESPONSE"
And the following checks should pass
| RECEIVED_RESPONSE.statusCode.name == MOCKSERVER_DEFAULT_RESPONSE.statusCode |
And "RECEIVED_RESPONSE" should contains "MOCKSERVER_DEFAULT_RESPONSE"
```

#### received response variable

this relies on register variable, "rest response", for the latest response.

BDD Example:

```yaml
And rest response should be "RECEIVED_RESPONSE"
```