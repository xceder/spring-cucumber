#### start server

```java
@And("^start mock server \"(.+)\"(?: at port (\\d+))?")
```

start the named mock server. the mockserver can be referred later. If the port is not specified, a random free port will
be used.

BDD Example:

```yaml
When start mock server "MOCK_RESPONSE_WEB" at port 8080
Given start mock server "MOCK_USAGE_WEB"
```

#### configure mockserver expectations

```java
@When("mock server {string} calls are configured") 
```

configure mockserver expectations. 

BDD Example:

```yaml
And mock server "MOCK_RESPONSE_WEB" calls are configured
| request | response | calls |
| REQ_2   | REP_1    | 1     |
```

#### assert mockserver expectations

```java
@Then("mock server {string} calls verification should {isSucceed}")
```

verify the mockserver calls

BDD Example:

```yaml
Then mock server "MOCK_USAGE_WEB" calls verification should fail
And mock server "MOCK_USAGE_WEB" calls verification should succeed
```

#### reset server

```java
@Given("reset mock server {string}")
```

reset the named mock server

BDD Example:

```yaml
When reset mock server "MOCK_USAGE_WEB"
```


