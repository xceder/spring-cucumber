#### define variables

```java
@Given("define the following variables")
```
it supports 2 types, doc string & datatable. the doc string should be yaml object. the key will be variable name. 
datatable will use "name" & "value" column for variable name and value. Yaml tag is used for the variable Java type, 
for example, "!!timestamp", "!RestRequest"

BDD Example:

```yaml
When define the following variables
"""
SIMPLE_STR: "string value"
INT: 1
IN_PLACE_EXPRESSION: "#{INT}"
TIMESTAMP: !!timestamp 2024-12-03T22:41:00Z
SPEL_MOCK_REQ_1: !RestRequest
  url: /
  method: GET
  headers:
    header1: ["req header1"]
    header2: ["req header2"]
    header3:
      - req header3
      - req header4

  queryParameters:
    param1: 1
    param2: param2
"""
And define the following variables
"""
OBJ:
  id: "#{INT}"
  value: "#{SIMPLE_STR}"
  list:
    - a: 1
      b:
        bb: 2

    - a: 3
      b:
        bb: 4

OBJ2:
  id: "#{INT}1"
  value: "#{SIMPLE_STR}"
"""
And define the following variables
| name           | value                    |
| EXPRESSION_STR | "test expression #{INT}" |
```

#### define variables using another variable 

```java
@Given("define variable {string} by{clone}{string}")
@Given("define variable {string} for{clone}{words}")
```
it will clone the specified variable with the new label. Then expression can use the new variable

BDD Example:

```yaml
When define variable "CLONE_OBJ" by clone "OBJ"
And define variable "object_variable" for object variable
Then object variable should equal to "object_variable"
And the following checks should pass
| #object_variable instanceOf T(java.util.Map) | SPel assigned variable |
| object_variable  == #object_variable         | root object property   |
```

#### execute the provided expressions

```java
@When("execute the following expressions")
```
execute the provided SpeL expression in the table first column. other column will be ignored 

BDD Example:

```yaml
And execute the following expressions
| NEW_OBJ.new_id = OBJ.id + " cloned" | assign clone id    |
| VAR_FROM_EXPRESSION = {1,2,3}       | assign root object |
| NEW_OBJ.new_list = {1,2,3}          | assign list        |
| CLONE_OBJ.new_value = null          | assign null        |
```

#### assert expressions

```java
@Then("^the following checks? should pass(?: for( each element in|) \"(.+)\")?") 
```

evaluate the expressions in datatable.

BDD Example:

```yaml
Then the following checks should pass
  | OBJ.id == 1                                  |
  | OBJ.value == "string value"                  |
  | OBJ.id == INT && OBJ.value == SIMPLE_STR     |
  | OBJ != OBJ2 && OBJ2.id == "11"               |
  | OBJ == OBJ2 OR OBJ2.id == "11"               |
  | SIMPLE_STR == "string value"                 |
  | EXPRESSION_STR == '"test expression 1"'      |
  | EXPRESSION_STR == '"test expression #{INT}"' |
  And the following checks should pass for "OBJ"
  | id == 1                 |
  | value == "string value" |
  | list.size() == 2        |
  And the following checks should pass for each element in "OBJ.list.![b]"
  | bb == 2 OR bb == 4 |
  And the following checks should pass for each element in "OBJ.list.![b.bb]"
  | #this > 0 |

```

#### assert comparison expression using doc string

```java
@And("{string}( should){not}(be ){comparison}")
```
compare the first expression (variable) with variable defined in doc string

BDD Example:

```yaml
And "CLONE_OBJ" <comparator>
"""
id: "#{IN_PLACE_EXPRESSION}"
value: "#{SIMPLE_STR}"
list:
  - a: 1
    b:
      bb: 2

  - a: 3
    b:
      bb: 4
"""

And "CLONE_OBJ" should not <comparator>
"""
id: "#{IN_PLACE_EXPRESSION}"
value: "#{SIMPLE_STR}"
list:
  - a: 1
    b:
      bb: 2

  - a: 3
    b:
      bb: 5
"""

Examples:
  | comparator |
  | equals to  |
  | be same as |
  | be         |
  | contains   |
  | matches    |
```

#### assert comparison expression

```java
@And("{string}( should){not}(be ){comparison} {string}")
```
compare the first expression, variable, with second

BDD Example:

```yaml
And "CLONE_OBJ" <comparator> "OBJ"
And "CLONE_OBJ" should <comparator> "OBJ"
And "OBJ" should not <comparator> "NEW_OBJ"

Examples:
  | comparator |
  | equals to  |
  | be same as |
  | be         |
  | contains   |
  | matches    |
```

#### register variable 

```java
@When("register variable {string} in cucumber step class")
public void registerVariable(String label) {
  variableResolvers.register(label, this);
}
```

VariableResolvers scenario scope bean is used to allow register variables. These can be used in the step directly

BDD Example:

```yaml
When register variable "registered variable" in cucumber step class
Then registered variable should <comparator> "REGISTER_VARIABLE_INIT_VALUE"
And registered variable should not <comparator> "REGISTER_VARIABLE_CHANGED_VALUE"
```
