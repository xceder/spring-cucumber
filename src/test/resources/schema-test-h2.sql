CREATE TABLE IF NOT EXISTS test_table
(
    id          INT,
    name        VARCHAR(20),
    number      INT       default 0,
    create_time TIMESTAMP default CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
    );

ALTER TABLE test_table
    ALTER COLUMN id RESTART WITH 3;
