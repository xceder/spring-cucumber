Feature: restful steps

  Background:
    Given start mock server "REST_MOCK_WEB"
    And define the following variables
    """
      RESTFUL_REQ:
        url: "http://localhost:#{REST_MOCK_WEB.localPort}"
        method: GET

      MOCKSERVER_DEFAULT_RESPONSE:
        statusCode: NOT_FOUND
    """

  Scenario: restful usage
    When submit rest request
    """
      url: "http://localhost:#{REST_MOCK_WEB.localPort}"
      method: GET
    """
    And rest response is stored as "RECEIVED_RESPONSE"
    Then "RECEIVED_RESPONSE" should contains "MOCKSERVER_DEFAULT_RESPONSE"
    And the following checks should pass
      | RECEIVED_RESPONSE.statusCode.name == MOCKSERVER_DEFAULT_RESPONSE.statusCode |

    When submit rest request "RESTFUL_REQ"
    Then "RECEIVED_RESPONSE" should contains
    """
      statusCode: NOT_FOUND
    """
    And rest response should be "RECEIVED_RESPONSE"

    When submit rest request "RESTFUL_REQ_2" as
    """
      url: "http://localhost:#{REST_MOCK_WEB.localPort}"
      method: GET
    """
    Then "RECEIVED_RESPONSE" should contains "MOCKSERVER_DEFAULT_RESPONSE"
    And the following checks should pass
      | RESTFUL_REQ_2.url == RESTFUL_REQ.url            |
      | RESTFUL_REQ_2.method.name == RESTFUL_REQ.method |

