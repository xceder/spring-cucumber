Feature: database steps

  Scenario: load database sql
    When "init_testdb.sql" is loaded into the database
    And query Table "TestEntity" with
    """
      id: 1
      name: init sql value
    """
    Then query results should equals to
    """
      - id: 1
        name: "init sql value"
        number: 0
        createTime: ${json-unit.any-string}
    """

  Scenario: database operations
    Given define the following variables
    """
      RECORD_1:
        name: database operations case
      RECORD_2:
        name: database operations case name changed
        number: 3
    """

    When Table "TestEntity" has record
      | name | database operations case |
    Then Table "TestEntity" should have record
    """
      name: database operations case
    """
    And Table "TestEntity" should not have record
    """
      name: "database operations case name changed"
    """
    And Table "TestEntity" should have record "RECORD_1"
    And Table "TestEntity" should not have record "RECORD_2"

    When query table "TestEntity" with "RECORD_1"
    And Table "TestEntity" has record
    """
      id: "#{#query_results[0].id}"
      name: "database operations case name changed"
    """
    Then Table "TestEntity" has record "RECORD_2"
    And Table "TestEntity" should not have record
    """
      name: database operations case
    """
    Then Table "TestEntity" should have record
    """
      !TestEntity
      name: database operations case name changed
    """
    And Table "TestEntity" should have record "RECORD_2"
    And Table "TestEntity" should not have record "RECORD_1"

    When query Table "TestEntity" with
    """
      name: database operations case name changed
      number: 3
    """
    Then query results should equals to
    """
      - name: database operations case name changed
        number: 3
        createTime:
        id: "#{#query_results[0].id}"
    """
    And query results should not equals to
    """
      - name: database operations case name changed
        number: "3"
        createTime:
        id: "#{#query_results[0].id}"
    """
    And query results should equals to
    """
      - !TestEntity
        name: database operations case name changed
        number: "3"
        id: "#{#query_results[0].id}"
    """

    When query Table "TestEntity" with
    """
      name:
    """
    Then query results should equals to
    """
      []
    """

  Scenario: query table
    Given define the following variables
    """
      RECORD_1:
        idVal:
          idVal: "1"
          nameVal: ""
        uuidVal: !UUID 550e8400-e29b-41d4-a716-446655440000
        strValue: "string value"
        intValue: 12

      RECORD_2:
        idVal:
          idVal: "1"
          nameVal: id
        uuidVal:
        strValue:
        intValue: 0
    """
    And save table "TestEmbeddedEntity" with "RECORD_1"
    And save table "TestEmbeddedEntity" with
      | idVal | {idVal: 1, nameVal: id} |
    And save table "TestEmbeddedEntity" with
      | idVal | "#{RECORD_2.idVal}" |

    When query table "TestEmbeddedEntity" with
      | idVal.idVal | "1" |
    Then query results should equals to
    """
      - idVal:
          idVal: "1"
          nameVal: ""
        uuidVal: !UUID 550e8400-e29b-41d4-a716-446655440000
        strValue: "string value"
        intValue: 12
      - idVal:
          idVal: "1"
          nameVal: id
        uuidVal:
        strValue:
        intValue: 0
    """
    And query results should equals to
    """
      - "#{RECORD_1}"
      - "#{RECORD_2}"
    """

    When query table "TestEmbeddedEntity" with
      | idVal | {idVal: "1"} |
    Then query results should equals to
    """
      - idVal:
          idVal: "1"
          nameVal: ""
        uuidVal: !UUID 550e8400-e29b-41d4-a716-446655440000
        strValue: "string value"
        intValue: 12
      - idVal:
          idVal: "1"
          nameVal: id
        uuidVal:
        strValue:
        intValue: 0
    """

    When query table "TestEmbeddedEntity" with
    """
      idVal.idVal: "1"
      idVal.nameVal: ""
    """
    Then query results should equals to
    """
      - idVal:
          idVal: "1"
          nameVal: ""
        uuidVal: !UUID 550e8400-e29b-41d4-a716-446655440000
        strValue: "string value"
        intValue: 12
    """
    And query results should equals to "{RECORD_1}"

    When query table "TestEmbeddedEntity" at 3 seconds timeout
    """
      idVal.idVal: "1"
      strValue:
    """
    Then query results should equals to
    """
      - idVal:
          idVal: "1"
          nameVal: id
        uuidVal:
        strValue:
        intValue: 0
    """
    And query results should equals to "{RECORD_2}"

    When query table "TestEmbeddedEntity" with "RECORD_2" at 3 seconds timeout
    Then query results should equals to "{RECORD_2}"

  Scenario Template: table temporal columns
    Given save table "TestTemporalEntity" with
    """
      id: <id>
      <column>: !<type> "<value>"
    """

    When query table "TestTemporalEntity" with
    """
      id: <id>
    """
    And query results should match
    """
      - id: <id>
        <column>: !<type> "<value>"
    """

    Examples:
      | id | column               | type           | value                     |
      | 1  | columnOffsetDateTime | OffsetDateTime | 2011-12-03T10:15:30+01:00 |
      | 2  | columnOffsetTime     | OffsetTime     | 10:15:30+01:00            |
      | 3  | columnLocalDateTime  | LocalDateTime  | 2011-12-03T10:15:30.123   |
      | 4  | columnLocalTime      | LocalTime      | 10:15:30.123              |
      | 5  | columnInstant        | Instant        | 2011-12-03T10:15:30Z      |
      | 6  | columnTimestamp      | !timestamp     | 2011-12-03T10:15:30+01:00 |

  Scenario: save table with variable definition
    When save table "TestEntity" as "TEST_ENTITY_VARIABLE" with
    """
      &testEntityVariable
      name: test entity variable
    """
    And query table "TestEntity" with
    """
      <<: *testEntityVariable
    """
    Then query results should equals to
    """
      - "#{TEST_ENTITY_VARIABLE}"
    """
    And query results first record should equals to "TEST_ENTITY_VARIABLE"
    And query results should equals to
    """
      - <<: *testEntityVariable
        id: ${json-unit.ignore}
    """
