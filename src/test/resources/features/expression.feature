Feature: spel expression steps

  Scenario: expression evaluate
    Given define the following variables
    """
      SIMPLE_STR: "string value"
      INT: 1
      IN_PLACE_EXPRESSION: "#{INT}"
      TIMESTAMP: !!timestamp 2024-12-03T22:41:00Z
      id: !UUID 550e8400-e29b-41d4-a716-446655440000

      SPEL_MOCK_REP_1: !RestResponse
        statusCode: OK
        headers:
          Content-Type: ["application/json"]
          header1: ["rep header1"]
          header2: ["rep header2"]
          header3:
            - rep header3
            - rep header4
        body:
            field_a: 1
            field_b:
              - a: a1
                b:
                  ba: 1
                  bb: 2
              - a: a2
                b:
                  ba: 3
                  bb: 4

      SPEL_MOCK_REQ_1: !RestRequest
        url: /
        method: GET
        headers:
          header1: ["req header1"]
          header2: ["req header2"]
          header3:
            - req header3
            - req header4

        queryParameters:
          param1: 1
          param2: param2

    """
    And define the following variables
    """
      OBJ:
        id: "#{IN_PLACE_EXPRESSION}"
        value: "#{SIMPLE_STR}"
        list:
          - a: 1
            b:
              bb: 2

          - a: 3
            b:
              bb: 4

      OBJ2:
        id: "#{INT}1"
        value: "#{SIMPLE_STR}"
        list:
          - a: 1
    """

    When define the following variables
      | name           | value                    |
      | EXPRESSION_STR | "test expression #{INT}" |
    Then the following checks should pass
      | OBJ.id == 1                                |
      | OBJ.value == "string value"                |
      | OBJ.id == INT && OBJ.value == SIMPLE_STR   |
      | OBJ != OBJ2 && OBJ2.id == "11"             |
      | OBJ == OBJ2 OR OBJ2.id == "11"             |
      | SIMPLE_STR == "string value"               |
      | EXPRESSION_STR == 'test expression 1'      |
      | EXPRESSION_STR == "test expression #{INT}" |
    And the following checks should pass for "OBJ"
      | id == 1                 |
      | value == "string value" |
      | list.size() == 2        |
    And the following checks should pass for each element in "OBJ.list.![b]"
      | bb == 2 OR bb == 4 |
    And the following checks should pass for each element in "OBJ.list.![b.bb]"
      | #this > 0 |

  Scenario Template: expression comparison
    Given define the following variables
    """
      SIMPLE_STR: "string value"
      INT: 1
      IN_PLACE_EXPRESSION: "#{INT}"
      TIMESTAMP: !!timestamp 2024-12-03T22:41:00Z
      id: !UUID 550e8400-e29b-41d4-a716-446655440000

      SPEL_MOCK_REP_1: !RestResponse
        statusCode: OK
        headers:
          Content-Type: ["application/json"]
          header1: ["rep header1"]
          header2: ["rep header2"]
          header3:
            - rep header3
            - rep header4
        body:
            field_a: 1
            field_b:
              - a: a1
                b:
                  ba: 1
                  bb: 2
              - a: a2
                b:
                  ba: 3
                  bb: 4

      SPEL_MOCK_REQ_1: !RestRequest
        url: /
        method: GET
        headers:
          header1: ["req header1"]
          header2: ["req header2"]
          header3:
            - req header3
            - req header4

        queryParameters:
          param1: 1
          param2: param2

    """
    And define the following variables
    """
      OBJ:
        id: "#{IN_PLACE_EXPRESSION}"
        value: "#{SIMPLE_STR}"
        list:
          - a: 1
            b:
              bb: 2

          - a: 3
            b:
              bb: 4

      OBJ2:
        id: "#{INT}1"
        value: "#{SIMPLE_STR}"
        list:
          - a: 1
    """

    When define variable "CLONE_OBJ" by clone "OBJ"
    And define variable "NEW_OBJ" by clone "OBJ"
    And execute the following expressions
      | NEW_OBJ.new_id = OBJ.id + " cloned"                                                           | assign clone id    |
      | VAR_FROM_EXPRESSION = {1,2,3}                                                                 | assign root object |
      | NEW_OBJ.new_list = {1,2,3}                                                                    | assign list        |
      | NEW_OBJ.new_value = null                                                                      | assign null        |
      | assertEqual(SPEL_MOCK_REQ_1.getClass().getSimpleName(), "RestRequest")                        | assert equals      |
      | assertEqual(SPEL_MOCK_REP_1.getClass().getName(), "com.xceder.cucumber.restful.RestResponse") | assert equals      |
      | assertInstanceOf(OBJ, "java.util.Map")                                                        | assert instance of |

    Then the following checks should pass
      | NEW_OBJ.new_id == "1 cloned"            |
      | NEW_OBJ.new_list == VAR_FROM_EXPRESSION |

    And "CLONE_OBJ" should be "OBJ"
    And "CLONE_OBJ" should be same as "OBJ"

    And "CLONE_OBJ" should not equals to
    """
      id: "#{IN_PLACE_EXPRESSION}"
      value: "#{SIMPLE_STR}"
      list:
        - a: 1.0  #compare value&type
          b:
            bb: 2

        - a: 3
          b:
            bb: 4
    """

    And "CLONE_OBJ" should not be same as
    """
      id: "#{IN_PLACE_EXPRESSION}"
      value: "#{SIMPLE_STR}"
      list:
        - a: 1.0 #compare value&type
          b:
            bb: 2

        - a: 3
          b:
            bb: 4
    """

    And "CLONE_OBJ" <comparator> "OBJ"
    And "CLONE_OBJ" should <comparator> "OBJ"
    And "OBJ" should not <comparator> "NEW_OBJ"

    And "CLONE_OBJ" <comparator>
    """
      id: "#{IN_PLACE_EXPRESSION}"
      value: "#{SIMPLE_STR}"
      list:
        - a: 1
          b:
            bb: 2

        - a: 3
          b:
            bb: 4
    """
    And "CLONE_OBJ" should <comparator>
    """
      id: "#{IN_PLACE_EXPRESSION}"
      value: "#{SIMPLE_STR}"
      list:
        - a: 1
          b:
            bb: 2

        - a: 3
          b:
            bb: 4
    """
    And "CLONE_OBJ" should not <comparator>
    """
      id: "#{IN_PLACE_EXPRESSION}"
      value: "#{SIMPLE_STR}"
      list:
        - a: 1
          b:
            bb: 2

        - a: 3
          b:
            bb: 5
    """

    Examples:
      | comparator |
      | equals to  |
      | be same as |
      | be         |
      | contains   |
      | matches    |

  Scenario Template: registered variable expression
    Given define the following variables
    """
      REGISTER_VARIABLE_INIT_VALUE: "init value"
      REGISTER_VARIABLE_CHANGED_VALUE: "value is changed"
    """
    When register variable "registered variable" in cucumber step class
    Then registered variable should <comparator> "REGISTER_VARIABLE_INIT_VALUE"
    And registered variable should not <comparator> "REGISTER_VARIABLE_CHANGED_VALUE"

    When modify the register variable value in cucumber step class
    Then registered variable should <comparator>
    """
      value is changed
    """
    And registered variable should not <comparator>
    """
      init value
    """

    Examples:
      | comparator |
      | equals to  |
      | be same as |
      | be         |
      | contains   |
      | matches    |

  Scenario: evaluate expression in datatable
    When define the following variables
      | v1 | 1            |
      | v2 | "#{v1 + 1}"  |
      | v3 | #{v1 + 1}    |
      | v4 | "1"          |
      | v5 | [1,2,3]      |
      | v6 | {a: 1, b: 2} |
    Then the following checks should pass
      | v1 == 1                                |
      | v2 == 2                                |
      | v3 == null                             |
      | v4 instanceOf T(String) &&  v4 == "1"  |
      | v5[0] == 1 && v5[1] == 2 && v5[2] == 3 |
      | v6.a == 1 && v6.b == 2                 |

  Scenario Template: Java temporal classes
    When define the following variables
    """
      temporalValue: !<type> "<value>"
    """
    Then the following checks should pass
      | temporalValue.toString() == "<value>"      |
      | temporalValue.class.simpleName == "<type>" |

    Examples:
      | type           | value                    |
      | OffsetDateTime | 2011-12-03T10:15:30.123Z |
      | OffsetTime     | 10:15:30+01:00           |
      | LocalDateTime  | 2011-12-03T10:15:30.123  |
      | LocalTime      | 10:15:30.123             |
      | Instant        | 2011-12-03T10:15:30Z     |

  Scenario: dynamic variable accessible in SPel expression
    Given define the following variables
    """
      TEST_OBJ:
        t1: test
        t2: 1
        t3: [1,2,3]
    """
    When cucumber step class register an object variable "object variable"
    And define variable "object_variable" for object variable
    Then object variable should equal to "object_variable"
    And the following checks should pass
      | #object_variable instanceOf T(java.util.Map) | SPel assigned variable |
      | object_variable  == #object_variable         | root object property   |

    When adjust the object variable "object variable" inside step function with "TEST_OBJ"
    Then "object_variable" should equal to "TEST_OBJ"
    And "#object_variable" should equal to "TEST_OBJ"

    When execute the following expressions
      | #object_variable.t1 = 'new value' |
    Then the following checks should pass
      | object_variable.t1 == 'new value' |

    And query results should be
    """
      []
    """

  Scenario: Yaml tag with expression
    When define the following variables
    """
      OFFSET_DATETIME: !OffsetDateTime "2024-12-19T00:00:00Z"
      V1: !OffsetDateTime "#{OFFSET_DATETIME.toString()}"
      V2: "2"
      V3: !Integer "#{V2}"
      V4: !Double "#{V2}"
      TIMESTAMP: !!timestamp 2024-12-03T22:41:00Z
      SQL_TIMESTAMP: !Timestamp "#{TIMESTAMP}"
      SQL_TIMESTAMP2: !Timestamp 2024-12-03T22:41:00Z
    """
    Then "V1" should equal to "OFFSET_DATETIME"
    And "V3" should equal to "2"
    And "V3" should not equal to "'2'"
    And "SQL_TIMESTAMP.toString()" should equal to "'2024-12-04 06:41:00.0'"
    And the following checks should pass
      | V3 instanceOf T(java.lang.Integer)              |
      | V4 instanceOf T(java.lang.Double)               |
      | V3 == 2                                         |
      | V3 != "2"                                       |
      | V3 == V4                                        |
      | SQL_TIMESTAMP instanceOf T(java.sql.Timestamp)  |
      | SQL_TIMESTAMP2 instanceOf T(java.sql.Timestamp) |
      | SQL_TIMESTAMP == SQL_TIMESTAMP2                 |


  Scenario: Yaml anchor
    When define the following variables
    """
      RECORD_1: &record1
        idVal: &idValAnchor
          idVal: "1"
          nameVal: ""
        uuidVal: !UUID 550e8400-e29b-41d4-a716-446655440000
        strValue: "string value"
        intValue: 12
    """
    And define the following variables
    """
      # anchor across steps
      RECORD_2:
        idVal:
          <<: *idValAnchor
          nameVal: new name
          "new field": new field
        v3: "#{RECORD_1.intValue}"
        strValue: new str value
    """
    And define the following variables
    """
      # merge variable
      RECORD_3:
        t: 1

      RECORD_3:
        <<: *record1
        strValue: override value by yaml merge
        v1: new property from yaml merge
    """
    Then "RECORD_3" should not equal to "RECORD_1"

    When define the following variables
    """
      # merge variable
      RECORD_4:
        <<: *record1
    """
    Then "RECORD_4" should equal to "RECORD_1"
    And "RECORD_2" should equal to
    """
      idVal:
        idVal: "1"
        nameVal: "new name"
        "new field": new field
      v3: 12
      strValue: new str value
    """
    And "RECORD_3" should equal to
    """
      idVal:
        idVal: "1"
        nameVal: ""
      uuidVal: 550e8400-e29b-41d4-a716-446655440000
      strValue: override value by yaml merge
      intValue: 12
      v1: new property from yaml merge
    """
    And "RECORD_3" should not be
    """
      idVal:
        idVal: "1"
        nameVal: ""
      uuidVal: 550e8400-e29b-41d4-a716-446655440000
      strValue: override value by yaml merge
      intValue: 12
      v1: new property from yaml merge
    """

  Scenario: Yaml lombok builder
    When define the following variables
    """
      LOMBOK1: !TestLombok1
        name: test1
        testLombok2: !TestLombok2
          name: test2

      LOMBOK2: !TestLombok2
        name: test2
    """
    Then "LOMBOK1.testLombok2" should equal to "LOMBOK2"


  Scenario: Json methods
    When define the following variables
    """
      POJO:
        a: 1
        b: b
        c: [1,2,3]
        d:
          aa: 1
          bb: bb
      JSON: "#{toJSON(POJO)}"

      JSON2: '{"a": 1, "b": b, "c": "c"}'
      JSON3:
        a: 1
        b: b
    """
    Then "JSON" should equal to "toJSON(POJO)"
    And "POJO" should equal to "fromJSON(JSON)"
    And "escapeJSON(JSON2)" should equal to
    """
      '"{\"a\": 1, \"b\": b, \"c\": \"c\"}"'
    """
    And "escapeJSON(JSON3)" should equal to
    """
      '"{\"a\":1,\"b\":\"b\"}"'
    """


  Scenario: spel UUID methods
    When define the following variables
    """
      UUID: "#{uuid()}"
      UUID1: "#{uuid(1)}"
      UUID3: "#{nextUUID()}"
    """
    Then "UUID" should equal to "uuid(getUniqueNumber(0))"
    And "UUID.toString()" should equal to "formatUUID(getUniqueNumber(0))"
    And "UUID1.toString()" should equal to "'00000000-0000-0000-0000-000000000001'"
    And "UUID3" should equal to "uuid()"
    And "generatedUUIDCount" should equal to "2"
    And "testRootObjectUUID()" should equal to "UUID3"


  Scenario: spel running number methods
    When define the following variables
    """
      START_NUMBER: "#{runningNumber}"
    """
    Then "START_NUMBER" should equal to "getUniqueNumber(0)"
    And "runningNumber" should equal to "getUniqueNumber(0)"
    And "nextRunningNumber()" should equal to "getUniqueNumber(0)+1"
