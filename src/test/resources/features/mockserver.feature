Feature: mockserver steps

  Background:
    Given define the following variables
    """
      MOCK_REQ_1:
        url: /
        method: GET
        headers:
          header1: ["req header1"]
          header2: ["req header2"]
          header3:
            - req header3
            - req header4

        queryParameters:
          param1: 1
          param2: param2

      MOCK_REQ_2:
        url: /path2
        method: POST

      MOCK_REQ_3:
        url: /path3
        method: POST
        headers:
          Content-Type: ["application/json"]
        body:
          p1: 1
          p2: 2

      MOCK_REP_1:
        statusCode: OK
        headers:
          Content-Type: ["application/json"]
          header1: ["rep header1"]
          header2: ["rep header2"]
          header3:
            - rep header3
            - rep header4
        body:
            field_a: 1
            field_b:
              - a: a1
                b:
                  ba: 1
                  bb: 2
              - a: a2
                b:
                  ba: 3
                  bb: 4

      MOCK_REP_NOT_FOUND:
        statusCode: NOT_FOUND
    """

  Scenario: verify the mockserver response
    When start mock server "MOCK_RESPONSE_WEB" at port 8080
    And mock server "MOCK_RESPONSE_WEB" calls are configured
      | request    | response   | calls |
      | MOCK_REQ_1 | MOCK_REP_1 | 1     |
    And submit rest request "MOCK_RESPONSE_REQUEST" as
    """
      url: "http://localhost:#{MOCK_RESPONSE_WEB.localPort+MOCK_REQ_1.url}"
      method: "#{MOCK_REQ_1.method}"
      headers: "#{MOCK_REQ_1.headers}"
      queryParameters: "#{MOCK_REQ_1.queryParameters}"
    """
    And rest response is stored as "MOCKSERVER_RECEIVED_RESPONSE"
    Then "MOCKSERVER_RECEIVED_RESPONSE" should contains "MOCK_REP_1"
    And the following checks should pass
      | MOCK_RESPONSE_REQUEST.queryParameters.param1 == 1      |
      | MOCK_RESPONSE_REQUEST.queryParameters.param1 != "1"    |
      | MOCKSERVER_RECEIVED_RESPONSE.statusCode.name() == "OK" |
    And the following checks should pass for "MOCKSERVER_RECEIVED_RESPONSE.body.get('field_b')"
      | [0].a == 'a1' && [1].a == 'a2' |
    And the following checks should pass for each element in "MOCKSERVER_RECEIVED_RESPONSE.body.field_b.![b]"
      | ba == 1 OR ba == 3 |

  Scenario Template: mockserver usage
    Given start mock server "MOCK_USAGE_WEB"
    And define the following variables
    """
    MOCK_REST_SUBMIT:
      url: "http://localhost:#{MOCK_USAGE_WEB.localPort+<request>.url}"
      method: "#{<request>.method}"
      headers: "#{<request>['headers']}"
      queryParameters: "#{<request>['queryParameters']}"
      body: "#{<request>['body']}"
    """

    When submit rest request "MOCK_REST_SUBMIT"
    And rest response is stored as "MOCKSERVER_USAGE_RESPONSE"
    Then "MOCKSERVER_USAGE_RESPONSE" should contains "MOCK_REP_NOT_FOUND"
    And mock server "MOCK_USAGE_WEB" calls verification should fail

    When mock server "MOCK_USAGE_WEB" calls are configured
      | request   | response   | calls   |
      | <request> | <response> | <calls> |
    Then mock server "MOCK_USAGE_WEB" calls verification should fail

    When submit rest request "MOCK_REST_SUBMIT"
    And rest response is stored as "MOCKSERVER_USAGE_RESPONSE"
    Then "MOCKSERVER_USAGE_RESPONSE" should contains "<response>"
    And mock server "MOCK_USAGE_WEB" calls verification should <verification result>

    When submit rest request "MOCK_REST_SUBMIT"
    And rest response is stored as "MOCKSERVER_USAGE_RESPONSE"
    Then "MOCKSERVER_USAGE_RESPONSE" should contains "<second request response>"

    When reset mock server "MOCK_USAGE_WEB"
    And submit rest request "MOCK_REST_SUBMIT"
    And rest response is stored as "MOCKSERVER_USAGE_RESPONSE"
    Then "MOCKSERVER_USAGE_RESPONSE" should contains "MOCK_REP_NOT_FOUND"

    Examples:
      | request    | response           | calls | verification result | second request response |
      | MOCK_REQ_1 | MOCK_REP_1         | 1     | succeed             | MOCK_REP_NOT_FOUND      |
      | MOCK_REQ_2 | MOCK_REP_NOT_FOUND |       | fail                | MOCK_REP_NOT_FOUND      |
      | MOCK_REQ_3 | MOCK_REP_1         | -1    | succeed             | MOCK_REP_1              |
