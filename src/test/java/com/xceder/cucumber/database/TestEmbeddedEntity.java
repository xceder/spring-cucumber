package com.xceder.cucumber.database;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import java.util.UUID;
import lombok.Data;

@Entity(name = "test_entity_name")
@Data
public class TestEmbeddedEntity {
  @EmbeddedId
  private TableId idVal;
  private UUID uuidVal;
  private String strValue;
  private int intValue;
}
