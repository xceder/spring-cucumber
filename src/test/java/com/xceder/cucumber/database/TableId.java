package com.xceder.cucumber.database;

import jakarta.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class TableId {
  private String idVal;
  private String nameVal;
}
