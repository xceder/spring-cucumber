package com.xceder.cucumber.database;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import lombok.Data;

@Entity
@Data
public class TestTemporalEntity {
  @Id
  private int id;
  private OffsetDateTime columnOffsetDateTime;
  private OffsetTime columnOffsetTime;
  private LocalDateTime columnLocalDateTime;
  private LocalDate columnLocalDate;
  private LocalTime columnLocalTime;
  private Instant columnInstant;
  private Timestamp columnTimestamp;
}
