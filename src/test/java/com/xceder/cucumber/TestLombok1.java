package com.xceder.cucumber;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class TestLombok1 {
  private String name;
  private String value;
  private TestLombok2 testLombok2;
}
