package com.xceder.cucumber;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.expression.ExpressionRoot;
import com.xceder.cucumber.expression.ScenarioCounter;
import io.cucumber.spring.ScenarioScope;
import java.util.UUID;
import java.util.function.Supplier;
import org.springframework.stereotype.Component;

@Component
@ScenarioScope
public class TestSpelRoot extends ExpressionRoot {
  public TestSpelRoot(Supplier<JsonMapper> jsonMapperSupplier,
                      ScenarioCounter scenarioCounter) {
    super(jsonMapperSupplier, scenarioCounter);
  }

  public UUID testRootObjectUUID() {
    return uuid();
  }
}
