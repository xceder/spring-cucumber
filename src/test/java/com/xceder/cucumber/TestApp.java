package com.xceder.cucumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(CucumberBeansConfig.class)
public class TestApp {
  public static void main(String[] args) {
    SpringApplication.run(TestApp.class, args);
  }
}
