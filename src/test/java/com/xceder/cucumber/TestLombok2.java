package com.xceder.cucumber;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class TestLombok2 {
  private String name;
  private String value;
}
