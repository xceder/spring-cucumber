package com.xceder.cucumber.steps.expression;

import com.xceder.cucumber.expression.ExpressionResolver;
import com.xceder.cucumber.expression.VariableResolvers;
import io.cucumber.java.en.When;
import java.util.HashMap;
import java.util.Map;

public class ExpressionTestStep implements ExpressionResolver<Object> {
  private static final String OBJ_VAR = "object variable";
  private final VariableResolvers variableResolvers;
  private transient String registerVariable = "init value";
  private final Map<String, Object> objectVariable = new HashMap<>();

  public ExpressionTestStep(VariableResolvers variableResolvers) {
    this.variableResolvers = variableResolvers;
  }

  @Override
  public Object resolve(String expression) {
    return expression.equals(OBJ_VAR) ? objectVariable : registerVariable;
  }

  @When("register variable {string} in cucumber step class")
  public void registerVariable(String label) {
    variableResolvers.register(label, this);
  }

  @When("modify the register variable value in cucumber step class")
  public void modifyTheRegisterVariableValueInCucumberStepClass() {
    registerVariable = "value is changed";
  }

  @When("cucumber step class register an object variable \"object variable\"")
  public void cucumberStepClassRegisterAnObjectVariable() {
    variableResolvers.register(OBJ_VAR, this);
  }

  @When("adjust the object variable \"object variable\" inside step function with {string}")
  public void adjustTheObjectVariableInsideStepFunction(String expression) {
    Map<String, ?> value = variableResolvers.resolve(expression, false);

    assert value != null;
    objectVariable.putAll(value);
  }
}
