package com.xceder.cucumber.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import org.springframework.http.HttpMethod;


public class HttpMethodSerializer extends StdSerializer<HttpMethod> {

  public HttpMethodSerializer() {
    super(HttpMethod.class);
  }

  @Override
  public void serialize(HttpMethod httpMethod,
                        JsonGenerator jsonGenerator,
                        SerializerProvider serializers) throws IOException {
    jsonGenerator.writeString(httpMethod.name());
  }
}