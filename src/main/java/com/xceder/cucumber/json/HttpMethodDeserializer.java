package com.xceder.cucumber.json;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.http.HttpMethod;

public class HttpMethodDeserializer extends StdDeserializer<HttpMethod> {
  public HttpMethodDeserializer() {
    super(HttpMethod.class);
  }

  @Override
  public HttpMethod deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JacksonException {
    return HttpMethod.valueOf(p.getText().toUpperCase());
  }
}
