package com.xceder.cucumber.database;

import com.xceder.cucumber.yaml.ClassResolver;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

public class EntityClassExecutor implements DBExecutor {
  private final ClassResolver resolver;

  @PersistenceContext
  private EntityManager entityManager;

  public EntityClassExecutor(ClassResolver resolver) {
    this.resolver = resolver;
  }

  @Override
  public Class<?> getEntityClass(String entityName) {
    return resolver.resolve(entityName);
  }

  @Override
  public EntityManager getEntityManager() {
    return entityManager;
  }
}
