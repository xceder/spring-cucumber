package com.xceder.cucumber.database;

import com.xceder.cucumber.expression.ExpressionResolverRegister;
import java.util.ArrayList;
import java.util.List;

public class DatabaseExpressionResolver implements ExpressionResolverRegister<Object> {
  public static final String QUERY_RESULTS = "query results";
  public static final String QUERY_RESULT_FIRST = "query results first record";

  private final List<Object> queryResults = new ArrayList<>();

  @Override
  public Object resolve(String expression) {
    return switch (expression) {
      case QUERY_RESULT_FIRST -> queryResults.isEmpty() ? null : queryResults.get(0);
      default -> queryResults;
    };
  }

  public void setQueryResults(List<?> queryResults) {
    this.queryResults.clear();
    this.queryResults.addAll(queryResults);
  }

  @Override
  public List<String> getSupportedExpressions() {
    return List.of(QUERY_RESULTS, QUERY_RESULT_FIRST);
  }
}
