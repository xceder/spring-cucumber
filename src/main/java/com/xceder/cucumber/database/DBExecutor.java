package com.xceder.cucumber.database;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

public interface DBExecutor {
  Class<?> getEntityClass(String entityName);

  EntityManager getEntityManager();

  default boolean exists(String entityName, Map<String, ?> entityMap) {
    return !find(entityName, entityMap).isEmpty();
  }

  @SuppressWarnings("unchecked")
  default <T> List<T> find(String entityName, Map<String, ?> filters) {
    var entityClass = getEntityClass(entityName);

    var tableName = entityClass.getAnnotation(Entity.class).name();
    if (tableName == null || tableName.isEmpty()) {
      tableName = entityClass.getSimpleName();
    }

    StringBuilder jpqlBuilder = new StringBuilder("SELECT a FROM  ")
        .append(tableName)
        .append(" a ");

    var where = buildWhere("", filters);

    Map<String, Object> paramMap = new HashMap<>();
    if (!where.isEmpty()) {

      var jqlFilterBuilder = new StringJoiner(" AND ");

      int paramIndex = 0;
      for (var entry : where.entrySet()) {
        var paramValue = entry.getValue();
        if (paramValue == null) {
          jqlFilterBuilder.add(entry.getKey() + " IS NULL ");
        } else {
          var paramName = String.format("P%d", ++paramIndex);
          paramMap.put(paramName, entry.getValue());
          jqlFilterBuilder.add(entry.getKey() + " = :" + paramName);
        }
      }

      jpqlBuilder.append(" WHERE ").append(jqlFilterBuilder);
    }

    var jql = jpqlBuilder.toString();
    var query = getEntityManager().createQuery(jql);

    paramMap.forEach(query::setParameter);

    return query.getResultList();
  }

  default <T> T save(T entity) {
    return getEntityManager().merge(entity);
  }

  @SuppressWarnings("unchecked")
  private Map<String, ?> buildWhere(String parentKey, Map<String, ?> filters) {
    Map<String, Object> resultMap = new HashMap<>();

    for (Map.Entry<String, ?> entry : filters.entrySet()) {
      String key = entry.getKey();
      Object value = entry.getValue();

      if (value instanceof Map<?, ?>) {
        var map = buildWhere(parentKey + key + ".", (Map<String, ?>) value);
        resultMap.putAll(map);
      } else {
        resultMap.put(parentKey + key, value);
      }
    }

    return resultMap;
  }
}
