package com.xceder.cucumber;

import static io.cucumber.junit.platform.engine.Constants.FILTER_TAGS_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.JUNIT_PLATFORM_NAMING_STRATEGY_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PARALLEL_EXECUTION_ENABLED_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PUBLISH_QUIET_PROPERTY_NAME;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;

@IncludeEngines("cucumber")
@SelectClasspathResource("features")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME,
    value = "pretty, timeline:build/reports/cucumber/timeline,"
        + "html:build/reports/cucumber/cucumber.html")
@ConfigurationParameter(key = JUNIT_PLATFORM_NAMING_STRATEGY_PROPERTY_NAME, value = "long")
@ConfigurationParameter(key = PLUGIN_PUBLISH_QUIET_PROPERTY_NAME, value = "true")
@ConfigurationParameter(key = PARALLEL_EXECUTION_ENABLED_PROPERTY_NAME, value = "true")
@ConfigurationParameter(key = FILTER_TAGS_PROPERTY_NAME, value = "not (@Disabled or @Ignore)")
@ConfigurationParameter(key = "cucumber.execution.exclusive-resources.GlobalLock.read-write",
    value = "org.junit.platform.engine.support.hierarchical.ExclusiveResource.GLOBAL_KEY")
@ConfigurationParameter(key = "cucumber.execution.exclusive-resources.Lock.read-write",
    value = "com.xceder.cucumber")
@ConfigurationParameter(key = "cucumber.execution.exclusive-resources.ReadLock.read",
    value = "com.xceder.cucumber")
public class CucumberJunitConfig {
}
