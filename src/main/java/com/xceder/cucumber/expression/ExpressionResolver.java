package com.xceder.cucumber.expression;

import javax.annotation.Nullable;

public interface ExpressionResolver<T> {
  @Nullable T resolve(String expression);
}
