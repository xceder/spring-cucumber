package com.xceder.cucumber.expression;

import java.util.List;

public interface ExpressionResolverRegister<T> extends ExpressionResolver<T> {
  List<String> getSupportedExpressions();
}
