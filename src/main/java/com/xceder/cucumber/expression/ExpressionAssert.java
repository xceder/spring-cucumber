package com.xceder.cucumber.expression;

import javax.annotation.Nullable;

public record ExpressionAssert(String condition) {
  public boolean checkCondition(@Nullable Object rootObject, CucumberSpelExpression expression) {
    var evalCondition = expression.evaluate(condition, true, rootObject).toString();
    var checkResult = expression.evaluate(evalCondition, false, rootObject);
    return Boolean.TRUE.equals(checkResult);
  }
}
