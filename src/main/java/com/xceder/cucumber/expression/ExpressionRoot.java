package com.xceder.cucumber.expression;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.expression.spelmethod.SpelAssertions;
import com.xceder.cucumber.expression.spelmethod.SpelJson;
import com.xceder.cucumber.expression.spelmethod.SpelRunningNumber;
import com.xceder.cucumber.expression.spelmethod.SpelUUID;
import java.io.Serial;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Supplier;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
public class ExpressionRoot extends HashMap<String, Object>
    implements SpelJson, SpelAssertions, SpelRunningNumber, SpelUUID {
  private final JsonMapper jsonMapper;

  @Serial
  private static final long serialVersionUID = 1L;

  private final transient ScenarioCounter scenarioCounter;
  @Getter
  private final int scenario;
  @Setter
  private String uuidFormat = "00000000-0000-0000-0000-%012d";
  @Getter
  private int generatedUUIDCount = 1;
  private int runningNumber = 0;

  public ExpressionRoot(Supplier<JsonMapper> jsonMapperSupplier,
                        ScenarioCounter scenarioCounter) {
    this.jsonMapper = jsonMapperSupplier.get();
    this.scenario = scenarioCounter.getScenarioNumber();
    this.scenarioCounter = scenarioCounter;
  }

  @Override
  public JsonMapper getJsonMapper() {
    return jsonMapper;
  }

  @Override
  public int nextRunningNumber() {
    ++runningNumber;
    return getRunningNumber();
  }

  public int getUniqueNumber(int number) {
    return scenarioCounter.getUniqueNumber(number);
  }

  @Override
  public int getRunningNumber() {
    return getUniqueNumber(runningNumber);
  }

  @Override
  public UUID uuid() {
    var number = getUniqueNumber(generatedUUIDCount - 1);
    return uuid(number);
  }

  @Override
  public UUID nextUUID() {
    generatedUUIDCount++;
    return uuid();
  }

  @Override
  public String formatUUID(int number) {
    return String.format(uuidFormat, number);
  }
}
