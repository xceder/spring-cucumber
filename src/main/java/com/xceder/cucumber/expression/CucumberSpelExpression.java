package com.xceder.cucumber.expression;

import com.fasterxml.jackson.databind.json.JsonMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import lombok.Getter;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.ClassUtils;

public class CucumberSpelExpression {
  private final SpelExpressionParser parser = new SpelExpressionParser();
  private final Map<Class<?>, ExpressionEval<?>> expressionEvalMap = new HashMap<>();

  @Getter
  private final StandardEvaluationContext context = new StandardEvaluationContext();
  private final JsonMapper jsonMapper;

  @SuppressFBWarnings("CT_CONSTRUCTOR_THROW")
  @SuppressWarnings("unchecked")
  public CucumberSpelExpression(ExpressionRoot expressionRoot) {
    context.addPropertyAccessor(new MapAccessor());
    context.setRootObject(expressionRoot);

    register(String.class, this::evaluate);
    register(Map.class, this::evaluate);
    register(Collection.class, this::evaluate);

    this.jsonMapper = expressionRoot.getJsonMapper();
  }

  public <T> void register(Class<T> valueType, ExpressionEval<T> expressionEval) {
    expressionEvalMap.put(valueType, expressionEval);
  }

  @SuppressWarnings("unchecked")
  public <T> Map<String, T> getRootObject() {
    return (Map<String, T>) context.getRootObject().getValue();
  }

  public Expression parse(String expression, boolean isTemplateExpression) {
    Expression parsedExpression;
    if (isTemplateExpression) {
      parsedExpression = parser.parseExpression(expression, ParserContext.TEMPLATE_EXPRESSION);
    } else {
      parsedExpression = parser.parseExpression(expression);
    }

    return parsedExpression;
  }

  private ExpressionEval<?> findEvalByClass(Class<?> valueClass) {
    var result = expressionEvalMap.get(valueClass);

    if (result == null) {
      var parentClass = valueClass.getSuperclass();
      if (parentClass != null && parentClass != Object.class) {
        result = findEvalByClass(parentClass);
      }

      if (result == null) {
        for (var type : ClassUtils.getAllInterfacesForClass(valueClass)) {
          if (type == valueClass) {
            continue;
          }

          result = findEvalByClass(type);
          if (result != null) {
            break;
          }
        }
      }

      if (result != null) {
        expressionEvalMap.put(valueClass, result);
      }
    }

    return result;
  }

  private boolean isExpressionCarrier(Class<?> type) {
    boolean result = false;

    if (!type.isPrimitive()) {
      for (var field : type.getFields()) {
        if (!field.getType().isPrimitive()) {
          var modifiers = field.getModifiers();

          if (!Modifier.isStatic(modifiers) && !Modifier.isFinal(modifiers)) {
            result = true;
            break;
          }
        }
      }
    }

    return result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public <T> T evaluate(@Nullable T value, boolean isTemplateExpression) {
    T result = value;

    if (value != null) {
      var valueClass = value.getClass();
      var expressionEval = (ExpressionEval<Object>) findEvalByClass(valueClass);

      if (expressionEval == null) {

        if (isExpressionCarrier(valueClass)) {
          var map = jsonMapper.convertValue(value, Map.class);
          var processedMap = evaluate(map, isTemplateExpression);
          result = (T) jsonMapper.convertValue(processedMap, value.getClass());
        }
      } else {
        result = (T) expressionEval.eval(value, isTemplateExpression);
      }
    }

    return result;
  }

  @SuppressWarnings("unchecked")
  public <T> T evaluate(String expression, Class<T> typeClass) {
    var expressionResult = evaluate(expression, false);

    T result;
    if (typeClass.isAssignableFrom(expressionResult.getClass())) {
      result = (T) expressionResult;
    } else {
      result = jsonMapper.convertValue(expressionResult, typeClass);
    }

    return result;
  }

  public Object evaluate(Collection<?> collection, boolean isTemplateExpression) {
    return collection.stream().map(v -> evaluate(v, isTemplateExpression)).toList();
  }

  public Map<String, ?> evaluate(Map<String, ?> expressMap, boolean isTemplateExpression) {
    Map<String, Object> resultMap = new HashMap<>();
    expressMap.forEach((k, v) -> resultMap.put(k, evaluate(v, isTemplateExpression)));

    return resultMap;
  }

  public Object evaluate(String expression, boolean isTemplateExpression) {
    return evaluate(expression, isTemplateExpression, null);
  }

  public Object evaluate(String expression,
                         boolean isTemplateExpression,
                         @Nullable Object rootObject) {
    var parsed = parse(expression, isTemplateExpression);

    Object result;
    if (rootObject == null) {
      result = parsed.getValue(context);
    } else {
      result = parsed.getValue(context, rootObject);
    }

    return result;
  }
}
