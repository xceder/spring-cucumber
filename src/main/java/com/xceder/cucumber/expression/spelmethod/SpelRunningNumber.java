package com.xceder.cucumber.expression.spelmethod;

public interface SpelRunningNumber {
  int nextRunningNumber();

  int getRunningNumber();
}
