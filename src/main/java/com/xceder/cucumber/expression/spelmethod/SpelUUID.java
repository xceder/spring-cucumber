package com.xceder.cucumber.expression.spelmethod;

import java.util.UUID;

public interface SpelUUID {
  UUID uuid();

  default UUID uuid(int number) {
    var id = formatUUID(number);
    return UUID.fromString(id);
  }

  UUID nextUUID();

  String formatUUID(int number);
}
