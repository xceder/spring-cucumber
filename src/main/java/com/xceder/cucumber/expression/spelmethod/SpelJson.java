package com.xceder.cucumber.expression.spelmethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.util.Map;

public interface SpelJson {
  JsonMapper getJsonMapper();

  default Map<String, ?> fromJSON(String json) throws JsonProcessingException {
    TypeReference<Map<String, Object>> typeRef = new TypeReference<>() {
    };
    return getJsonMapper().readValue(json, typeRef);
  }

  default String toJSON(Object object) throws JsonProcessingException {
    return getJsonMapper().writeValueAsString(object);
  }

  default String escapeJSON(Object object) throws JsonProcessingException {
    var result = toJSON(object);
    if (!(object instanceof String)) {
      result = toJSON(result);
    }
    return result;
  }
}
