package com.xceder.cucumber.expression.spelmethod;

import static org.assertj.core.api.Assertions.assertThat;

public interface SpelAssertions {
  default void assertEqual(Object actual, Object expected) {
    assertThat(actual).isEqualTo(expected);
  }

  default void assertInstanceOf(Object actual, String className) throws ClassNotFoundException {
    assertThat(actual).isInstanceOf(Class.forName(className));
  }
}
