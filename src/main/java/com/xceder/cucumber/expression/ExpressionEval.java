package com.xceder.cucumber.expression;

public interface ExpressionEval<T> {
  Object eval(T expression, boolean isTemplateExpression);
}
