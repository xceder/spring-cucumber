package com.xceder.cucumber.expression;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;

public class VariableResolvers {
  private final CucumberSpelExpression expressionEngine;
  private final Map<String, ExpressionResolver<?>> variableResolverMap = new HashMap<>();

  public VariableResolvers(CucumberSpelExpression expressionEngine) {
    this.expressionEngine = expressionEngine;
  }

  public static String escapeSpelVariableName(String input) {
    String sanitized = input.replaceAll("[^a-zA-Z0-9_$]", "_");

    // Ensure the first character is a valid start character
    if (!Character.isJavaIdentifierStart(sanitized.charAt(0))) {
      sanitized = "_" + sanitized;
    }

    return sanitized;
  }

  @Nullable
  @SuppressWarnings("unchecked")
  public <T> T resolve(String expression, boolean isOnlyRegisterVariable) {
    Object result;
    var resolver = variableResolverMap.get(expression);

    if (resolver == null) {
      if (isOnlyRegisterVariable) {
        throw new UnsupportedOperationException("variable \"" + expression + "\" isn't registered");
      } else {
        result = expressionEngine.evaluate(expression, false);
      }
    } else {
      result = resolver.resolve(expression);
      setVariable(expression, result);
    }

    return (T) result;
  }

  public void register(String expression, ExpressionResolver<?> resolver) {
    variableResolverMap.put(expression, resolver);
    setVariable(expression, resolver.resolve(expression));
  }

  private void setVariable(String expression, Object result) {
    var varName = escapeSpelVariableName(expression);
    expressionEngine.getContext().setVariable(varName, result);
  }
}
