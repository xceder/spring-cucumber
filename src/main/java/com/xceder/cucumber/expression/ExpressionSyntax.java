package com.xceder.cucumber.expression;

public enum ExpressionSyntax {
  DIRECT,
  TEMPLATE,
  NONE
}
