package com.xceder.cucumber.expression;

import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;

@Getter
public class ScenarioCounter {
  private static final AtomicInteger counter = new AtomicInteger(0);
  private final int scenarioNumber;

  public ScenarioCounter() {
    this.scenarioNumber = counter.incrementAndGet();
  }

  public int getUniqueNumber(int number) {
    return (getScenarioNumber() - 1) * 1_000_000 + number;
  }
}
