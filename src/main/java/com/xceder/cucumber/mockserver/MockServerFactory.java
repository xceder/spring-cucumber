package com.xceder.cucumber.mockserver;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.mockserver.integration.ClientAndServer;

public class MockServerFactory {
  private static final Map<String, ClientAndServer> mockServerInstances = new ConcurrentHashMap<>();

  private int getFreePort() throws IOException {
    try (ServerSocket socket = new ServerSocket(0)) {
      return socket.getLocalPort();
    }
  }

  @Nullable
  public ClientAndServer getMockServer(String label) {
    return mockServerInstances.get(label);
  }

  @Nullable
  public ClientAndServer getMockServer(int listenPort) {
    return mockServerInstances.values().stream()
        .filter(server -> server.getLocalPorts().contains(listenPort))
        .findFirst()
        .orElse(null);
  }

  public ClientAndServer startMockServer(String label) throws IOException {
    var server = getMockServer(label);

    if (server == null) {
      var listenPort = getFreePort();
      server = startClientAndServer(listenPort);
      mockServerInstances.put(label, server);
    }

    return server;
  }

  public ClientAndServer startMockServer(String label, int listenPort) throws IOException {
    ClientAndServer server;

    if (listenPort <= 0) {
      server = startMockServer(label);
    } else {
      var existServer = getMockServer(label);
      if (existServer == null) {
        server = getMockServer(listenPort);

        if (server != null) {
          mockServerInstances.put(label, server);
        }
      } else if (existServer.getLocalPorts().contains(listenPort)) {
        server = existServer;
      } else {
        var portLine = existServer.getLocalPorts().stream()
            .map(String::valueOf)
            .collect(Collectors.joining(","));
        var error = String.format("server '%s' already listen at ports:%s", label, portLine);
        throw new IOException(error);
      }

      if (server == null) {
        server = startClientAndServer(listenPort);
        mockServerInstances.put(label, server);
      }
    }
    return server;
  }
}
