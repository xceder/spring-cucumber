package com.xceder.cucumber.mockserver;

public record MockRequestCalls(String request, int calls, String response) {

}