package com.xceder.cucumber.yaml;

import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Node;

public class ConstructorTagClass extends Constructor {
  private final YamlTagClassResolver resolver;

  public ConstructorTagClass(YamlTagClassResolver resolver) {
    super(new LoaderOptions());
    this.resolver = resolver;
    loadingConfig.setTagInspector(tag -> true);
  }

  protected Class<?> getClassForNode(Node node) {
    var tag = node.getTag();
    if (!typeTags.containsKey(tag) && tag.isSecondary()) {
      var className = node.getTag().getValue().replaceFirst("!", "");
      var tagClass = resolver.resolve(className);
      if (tagClass != null) {
        typeTags.put(node.getTag(), tagClass);
      }
    }
    return super.getClassForNode(node);
  }
}
