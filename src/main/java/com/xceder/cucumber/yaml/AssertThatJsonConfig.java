package com.xceder.cucumber.yaml;

import net.javacrumbs.jsonunit.assertj.JsonAssert;
import net.javacrumbs.jsonunit.core.Option;

public interface AssertThatJsonConfig {
  default JsonAssert.ConfigurableJsonAssert configForEqual(
      JsonAssert.ConfigurableJsonAssert jsonAssert) {
    return jsonAssert.when(Option.IGNORING_ARRAY_ORDER)
        .when(Option.TREATING_NULL_AS_ABSENT);
  }

  default JsonAssert.ConfigurableJsonAssert configForContain(
      JsonAssert.ConfigurableJsonAssert jsonAssert) {
    return jsonAssert.when(Option.IGNORING_ARRAY_ORDER)
        .when(Option.TREATING_NULL_AS_ABSENT)
        .when(Option.IGNORING_EXTRA_ARRAY_ITEMS)
        .when(Option.IGNORING_EXTRA_FIELDS);
  }
}
