package com.xceder.cucumber.yaml.types;

import java.util.function.Function;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;


public abstract class TemporalTypeDescription extends TypeDescription {
  protected final Function<String, Object> parser;

  public TemporalTypeDescription(Class<?> clazz, Function<String, Object> parser) {
    super(clazz);
    this.parser = parser;
  }

  public Object newInstance(Node node) {
    String value = ((ScalarNode) node).getValue();
    return parser.apply(value);
  }
}
