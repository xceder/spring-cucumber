package com.xceder.cucumber.yaml.types;

import java.time.Instant;
import org.springframework.stereotype.Component;

@Component
public class InstantTypeDescription extends TemporalTypeDescription {
  public InstantTypeDescription() {
    super(Instant.class, Instant::parse);
  }
}
