package com.xceder.cucumber.yaml.types;

import java.time.LocalDateTime;
import org.springframework.stereotype.Component;

@Component
public class LocalDateTimeTypeDescription extends TemporalTypeDescription {
  public LocalDateTimeTypeDescription() {
    super(LocalDateTime.class, LocalDateTime::parse);
  }
}
