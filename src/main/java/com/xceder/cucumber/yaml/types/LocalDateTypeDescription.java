package com.xceder.cucumber.yaml.types;

import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
public class LocalDateTypeDescription extends TemporalTypeDescription {
  public LocalDateTypeDescription() {
    super(LocalDate.class, LocalDate::parse);
  }
}
