package com.xceder.cucumber.yaml.types;

import java.time.LocalTime;
import org.springframework.stereotype.Component;

@Component
public class LocalTimeTypeDescription extends TemporalTypeDescription {
  public LocalTimeTypeDescription() {
    super(LocalTime.class, LocalTime::parse);
  }
}
