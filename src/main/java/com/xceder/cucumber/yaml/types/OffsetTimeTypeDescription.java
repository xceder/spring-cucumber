package com.xceder.cucumber.yaml.types;

import java.time.OffsetTime;
import org.springframework.stereotype.Component;

@Component
public class OffsetTimeTypeDescription extends TemporalTypeDescription {
  public OffsetTimeTypeDescription() {
    super(OffsetTime.class, OffsetTime::parse);
  }
}
