package com.xceder.cucumber.yaml.types;

import java.time.OffsetDateTime;
import org.springframework.stereotype.Component;

@Component
public class OffsetDateTimeTypeDescription extends TemporalTypeDescription {
  public OffsetDateTimeTypeDescription() {
    super(OffsetDateTime.class, OffsetDateTime::parse);
  }
}
