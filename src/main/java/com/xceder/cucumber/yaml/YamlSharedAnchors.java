package com.xceder.cucumber.yaml;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import lombok.SneakyThrows;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.composer.Composer;
import org.yaml.snakeyaml.constructor.BaseConstructor;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.parser.ParserImpl;
import org.yaml.snakeyaml.reader.StreamReader;
import org.yaml.snakeyaml.representer.Representer;

public class YamlSharedAnchors extends Yaml {
  private final Map<String, Node> sharedAnchors = new HashMap<>() {
    @Override
    public void clear() {

    }
  };

  @SuppressWarnings("deprecation")
  public YamlSharedAnchors(BaseConstructor constructor, Representer representer) {
    super(constructor, representer);
  }

  @SneakyThrows
  private Composer createSharedAnchorComposer(Reader reader) {
    Composer composer = new Composer(new ParserImpl(new StreamReader(reader), loadingConfig),
        resolver, loadingConfig);

    var field = Composer.class.getDeclaredField("anchors");

    field.setAccessible(true);
    field.set(composer, sharedAnchors);

    return composer;
  }

  public Node compose(Reader reader) {
    var composer = createSharedAnchorComposer(reader);
    return composer.getSingleNode();
  }


  @Override
  public <T> T load(String yaml) {
    return loadAs(yaml, Object.class);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T loadAs(String yaml, Class<? super T> type) {
    var composer = createSharedAnchorComposer(new StringReader(yaml));
    constructor.setComposer(composer);
    return (T) constructor.getSingleData(type);
  }
}
