package com.xceder.cucumber.yaml;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.expression.ExpressionSyntax;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.function.Supplier;
import javax.annotation.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;

public class YamlExpressionParser implements YamlParser {
  private final YamlSharedAnchors yamlEngine;
  private final ConstructorExpression constructorExpression;

  public YamlExpressionParser(Supplier<JsonMapper> jsonMapperSupplier,
                              YamlTagClassResolver yamlTagClassResolver,
                              CucumberSpelExpression expressionEngine,
                              ObjectProvider<TypeDescription> typeDescriptionProvider) {
    this.constructorExpression = new ConstructorExpression(expressionEngine,
        jsonMapperSupplier.get(),
        yamlTagClassResolver);
    this.yamlEngine = new YamlSharedAnchors(constructorExpression,
        new RepresentTemporal(typeDescriptionProvider));
    typeDescriptionProvider.forEach(yamlEngine::addTypeDescription);
  }

  @Override
  public <T> T load(String yaml, @Nullable Class<T> type) {
    return loadExpression(yaml, ExpressionSyntax.NONE, type);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> T load(Node node) {
    var buffer = new StringWriter();
    yamlEngine.serialize(node, buffer);
    return (T) loadExpression(buffer.toString(), ExpressionSyntax.TEMPLATE, node.getType());
  }

  @SuppressWarnings("unchecked")
  public Map<String, ?> loadExpression(String yaml) {
    return loadExpression(yaml, ExpressionSyntax.TEMPLATE, Map.class);
  }

  public <T> T loadExpression(String yaml, ExpressionSyntax mode) {
    return loadExpression(yaml, mode, null);
  }

  public <T> T loadExpression(String yaml, ExpressionSyntax mode, @Nullable Class<T> classType) {
    constructorExpression.setParseMode(mode);
    T result;
    if (classType == null) {
      result = yamlEngine.load(yaml);
    } else {
      result = yamlEngine.loadAs(yaml, classType);
    }

    return result;
  }

  @Override
  public Yaml getYamlEngine() {
    return yamlEngine;
  }

  public Node compose(String yaml) {
    return yamlEngine.compose(new StringReader(yaml));
  }
}
