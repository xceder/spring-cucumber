package com.xceder.cucumber.yaml;

public interface ClassResolver {
  Class<?> resolve(String nameSuffix);
}
