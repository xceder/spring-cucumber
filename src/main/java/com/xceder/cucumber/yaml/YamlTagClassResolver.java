package com.xceder.cucumber.yaml;

public interface YamlTagClassResolver {
  Class<?> resolve(String tag);
}
