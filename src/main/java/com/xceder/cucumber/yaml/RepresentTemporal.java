package com.xceder.cucumber.yaml;

import com.xceder.cucumber.yaml.types.TemporalTypeDescription;
import org.springframework.beans.factory.ObjectProvider;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

public class RepresentTemporal extends Representer {
  public RepresentTemporal(ObjectProvider<TypeDescription> typeDescriptionProvider) {
    super(new DumperOptions());

    typeDescriptionProvider.forEach(typeDescription -> {
      if (typeDescription instanceof TemporalTypeDescription) {
        var type = typeDescription.getType();
        representers.put(type, data -> createNode(type, data.toString()));
      }
    });
  }

  private Node createNode(Class<?> type, String value) {
    return new ScalarNode(new Tag(type), value, null, null, defaultScalarStyle);
  }
}
