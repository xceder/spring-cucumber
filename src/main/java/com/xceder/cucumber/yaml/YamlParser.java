package com.xceder.cucumber.yaml;

import java.io.StringWriter;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;

public interface YamlParser {
  Yaml getYamlEngine();

  default <T> T load(Node node) {
    var buffer = new StringWriter();
    getYamlEngine().serialize(node, buffer);
    return load(buffer.toString());
  }

  default <T> T load(String yaml, Class<T> type) {
    return getYamlEngine().loadAs(yaml, type);
  }

  default <T> T load(String yaml) {
    return getYamlEngine().load(yaml);
  }

  @SuppressWarnings("unchecked")
  default <T> T clone(T src) {
    var yaml = getYamlEngine().dump(src);
    return (T) load(yaml);
  }
}
