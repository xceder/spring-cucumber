package com.xceder.cucumber.yaml;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.expression.ExpressionSyntax;
import java.util.Map;
import javax.annotation.Nullable;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.NodeId;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;

public class ConstructorExpression extends ConstructorTagClass {
  private final CucumberSpelExpression expressionEngine;
  private final JsonMapper jsonMapper;
  private transient ExpressionSyntax mode = ExpressionSyntax.TEMPLATE;

  public ConstructorExpression(CucumberSpelExpression expressionEngine,
                               JsonMapper jsonMapper,
                               YamlTagClassResolver yamlTagClassResolver) {
    super(yamlTagClassResolver);
    this.expressionEngine = expressionEngine;
    this.jsonMapper = jsonMapper;

    this.yamlClassConstructors.put(NodeId.scalar, new ScalarConstructor());
    this.yamlClassConstructors.put(NodeId.mapping, new MapConstructor());
    this.yamlConstructors.put(Tag.STR, new TagStrConstructor());
  }

  public void setParseMode(ExpressionSyntax mode) {
    this.mode = mode;
  }

  private @Nullable Object evalNode(Node node) {
    var originValue = ((ScalarNode) node).getValue();
    return evalValue(originValue);
  }

  private @Nullable Object evalValue(String nodeValue) {
    return switch (mode) {
      case DIRECT -> expressionEngine.evaluate(nodeValue, false);
      case TEMPLATE -> expressionEngine.evaluate(nodeValue, true);
      default -> nodeValue;
    };
  }


  private class MapConstructor extends ConstructMapping {
    @Override
    public Object construct(Node node) {
      Object result;

      try {
        result = super.construct(node);
      } catch (YAMLException e) {
        result = constructTo((MappingNode) node);
      }

      return result;
    }

    private Object constructTo(MappingNode node) {
      var origClass = node.getType();
      node.setType(Map.class);
      var map = super.construct(node);
      return jsonMapper.convertValue(map, origClass);
    }
  }

  private class ScalarConstructor extends ConstructScalar {
    public Object construct(Node node) {
      Class<?> type = node.getType();
      Object value = null;
      var originValue = ((ScalarNode) node).getValue();

      if (originValue != null) {
        value = evalValue(originValue);

        if (value != null) {
          var valueType = value.getClass();

          if (!type.isAssignableFrom(valueType)) {
            value = jsonMapper.convertValue(value, type);
          }
        }
      }

      return value;
    }
  }

  private class TagStrConstructor extends ConstructYamlStr {
    public Object construct(Node node) {
      return evalNode(node);
    }
  }
}
