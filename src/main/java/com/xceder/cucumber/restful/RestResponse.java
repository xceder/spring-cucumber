package com.xceder.cucumber.restful;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.springframework.http.HttpStatus;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Jacksonized
@ToString(callSuper = true)
@NoArgsConstructor
public class RestResponse extends RestEntity {
  private HttpStatus statusCode;
}
