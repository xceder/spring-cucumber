package com.xceder.cucumber.restful;

import com.xceder.cucumber.expression.ExpressionResolverRegister;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nullable;

public class RestRequestResponseRecords extends LinkedList<RestRequestResponse> implements
    ExpressionResolverRegister<RestResponse> {
  private static final String REST_RESPONSE = "rest response";

  public void setCurrentRequest(RestRequest request) {
    var record = RestRequestResponse.builder()
        .request(request)
        .build();

    offerFirst(record);
  }

  public void setCurrentRequestResponse(RestResponse response) {
    var current = removeFirst();
    var record = RestRequestResponse.builder()
        .request(current.getRequest())
        .response(response)
        .build();

    offerFirst(record);
  }

  @Nullable
  public RestResponse getCurrentRequestResponse() {
    var record = peekFirst();
    RestResponse result = null;
    if (record != null) {
      result = record.getResponse();
    }

    return result;
  }

  @Override
  public List<String> getSupportedExpressions() {
    return List.of(REST_RESPONSE);
  }

  @Nullable
  @Override
  public RestResponse resolve(String expression) {
    return getCurrentRequestResponse();
  }
}
