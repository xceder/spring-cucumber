package com.xceder.cucumber.restful;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.net.URI;
import java.util.Map;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

@Data
@JsonIgnoreProperties
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class RestRequest extends RestEntity {
  private String url;
  @Nullable
  private HttpMethod method;
  @Nullable
  private Map<String, ?> queryParameters;

  public URI getURI() {
    var builder = UriComponentsBuilder.fromUri(URI.create(url));

    if (queryParameters != null) {
      queryParameters.forEach((k, v) -> {
        if (v != null) {
          builder.queryParam(k, v);
        }
      });
    }

    return builder.build().toUri();
  }
}