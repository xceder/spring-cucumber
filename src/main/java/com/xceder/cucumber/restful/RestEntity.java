package com.xceder.cucumber.restful;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.annotation.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpHeaders;

@Data
@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
@SuperBuilder
@NoArgsConstructor
public class RestEntity {
  @Nullable
  private HttpHeaders headers;

  @Nullable
  private Object body;
}
