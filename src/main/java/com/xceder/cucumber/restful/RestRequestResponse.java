package com.xceder.cucumber.restful;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RestRequestResponse {
  private RestRequest request;
  private RestResponse response;
}
