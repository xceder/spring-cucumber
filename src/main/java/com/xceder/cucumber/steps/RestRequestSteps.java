package com.xceder.cucumber.steps;

import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.restful.RestRequest;
import com.xceder.cucumber.restful.RestRequestResponseRecords;
import com.xceder.cucumber.restful.RestResponse;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;

public class RestRequestSteps {
  private final TestRestTemplate restTemplate;
  private final RestRequestResponseRecords restRequestResponseRecords;

  private final CucumberSpelExpression expressionEngine;

  public RestRequestSteps(TestRestTemplate restTemplate,
                          RestRequestResponseRecords restRequestResponseRecords,
                          CucumberSpelExpression expressionEngine) {
    this.restTemplate = restTemplate;
    this.restRequestResponseRecords = restRequestResponseRecords;
    this.expressionEngine = expressionEngine;
  }

  @When("submit rest request")
  public void submitRequest(RestRequest request) {
    restRequestResponseRecords.setCurrentRequest(request);

    var entity = new RequestEntity<>(request.getBody(),
        request.getHeaders(),
        request.getMethod(),
        request.getURI()
    );

    var response = restTemplate.exchange(entity, Object.class);
    var lastReceivedResponseEntity = RestResponse.builder()
        .headers(response.getHeaders())
        .body(response.getBody())
        .statusCode((HttpStatus) response.getStatusCode())
        .build();

    restRequestResponseRecords.setCurrentRequestResponse(lastReceivedResponseEntity);

    expressionEngine.getRootObject()
        .put("restRequest", request);
    expressionEngine.getRootObject()
        .put("restResponse", lastReceivedResponseEntity);
  }

  @When("submit rest request {string}")
  public void submitRequest(String expression) {
    var request = expressionEngine.evaluate(expression, RestRequest.class);
    submitRequest(request);
  }

  @When("submit rest request {string} as")
  public void submitRequest(String label, RestRequest request) {
    expressionEngine.getRootObject().put(label, request);
    submitRequest(request);
  }

  /**
   *
   * Use {@link com.xceder.cucumber.steps.ExpressionSteps#defineVariables(String newVar, boolean isClone, String targetExpression)  } instead.
   */
  @Deprecated(forRemoval = true)
  @And("rest response is stored as {string}")
  public void labelRestResponseAs(String label) {
    expressionEngine.getRootObject().put(label,
        restRequestResponseRecords.getCurrentRequestResponse());
  }
}
