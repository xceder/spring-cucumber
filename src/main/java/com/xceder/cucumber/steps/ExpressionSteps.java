package com.xceder.cucumber.steps;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.JSON;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.expression.ExpressionAssert;
import com.xceder.cucumber.expression.ExpressionSyntax;
import com.xceder.cucumber.expression.VariableResolvers;
import com.xceder.cucumber.yaml.AssertThatJsonConfig;
import com.xceder.cucumber.yaml.YamlExpressionParser;
import io.cucumber.java.DataTableType;
import io.cucumber.java.DocStringType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import javax.annotation.Nullable;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.Tag;

public class ExpressionSteps {
  private final CucumberSpelExpression expressionEngine;
  private final VariableResolvers variableResolvers;
  private final YamlExpressionParser yamlExpressionParser;
  private final JsonMapper jsonMapper;
  private final AssertThatJsonConfig assertJsonConfig;

  public ExpressionSteps(CucumberSpelExpression expressionEngine,
                         VariableResolvers variableResolvers,
                         YamlExpressionParser yamlExpressionParser,
                         Supplier<JsonMapper> jsonMapperSupplier,
                         AssertThatJsonConfig assertJsonConfig) {
    this.expressionEngine = expressionEngine;
    this.yamlExpressionParser = yamlExpressionParser;
    this.variableResolvers = variableResolvers;
    this.jsonMapper = jsonMapperSupplier.get();
    this.assertJsonConfig = assertJsonConfig;
  }

  @DataTableType
  @Nullable
  public Object transformExpressionCell(@Nullable String cell) {
    Object result = cell;
    if (cell != null && !cell.isBlank()) {
      result = yamlExpressionParser.loadExpression(cell, ExpressionSyntax.TEMPLATE);
    }

    return result;
  }

  @DocStringType
  public Map<String, ?> load(String yaml) {
    return yamlExpressionParser.loadExpression(yaml);
  }


  @DataTableType
  public ExpressionAssert convert(List<String> entry) {
    return new ExpressionAssert(entry.get(0));
  }

  @DataTableType
  public NodeTuple convertYamlNodes(List<String> row) {
    assertThat(row.size()).isGreaterThanOrEqualTo(2);
    var key = row.get(0);
    var value = yamlExpressionParser.loadExpression(row.get(1), ExpressionSyntax.NONE);

    var yaml = yamlExpressionParser.getYamlEngine();
    var keyNode = yaml.represent(key);
    var valueNode = yaml.represent(value);

    return new NodeTuple(keyNode, valueNode);
  }

  @DocStringType
  public List<NodeTuple> convertYamlNodes(String yaml) {
    var node = yamlExpressionParser.compose(yaml);
    assertThat(node.getTag()).isEqualTo(Tag.MAP);

    return ((MappingNode) node).getValue();
  }

  @Given("define the following variables")
  public void defineVariables(List<NodeTuple> variableDefList) {

    for (var nodeTuple : variableDefList) {
      var key = yamlExpressionParser.load(nodeTuple.getKeyNode());
      var value = yamlExpressionParser.load(nodeTuple.getValueNode());
      expressionEngine.getRootObject().put(key.toString(), value);
    }
  }

  @Given("define variable {string} by{clone}{string}")
  @Given("define variable {string} for{clone}{words}")
  @Given("define variable {string} for{clone}{string}")
  public void defineVariables(String newVar, boolean isClone, String targetExpression) {
    var target = variableResolvers.resolve(targetExpression, false);
    var newObj = isClone ? yamlExpressionParser.clone(target) : target;
    expressionEngine.getRootObject().put(newVar, newObj);
  }

  @When("execute the following expressions")
  public void runExpressions(List<List<String>> expressionLists) {
    assertThat(expressionLists).isNotEmpty();

    for (var list : expressionLists) {
      expressionEngine.evaluate(list.get(0), false);
    }
  }

  @Then("^the following checks? should pass(?: for( each element in|) \"(.+)\")?")
  public void passChecks(@Nullable String loop, @Nullable String elementExpression,
                         List<ExpressionAssert> expectList) {
    assertThat(expectList).describedAs("the following check(s) should pass").isNotEmpty()
        .allSatisfy(e -> {
          Object element;
          if (elementExpression != null) {
            element = expressionEngine.evaluate(elementExpression, false);
          } else {
            element = null;
          }

          if (" each element in".equals(loop) && element instanceof Iterable<?>) {
            ((Iterable<?>) element).forEach(node -> {
              var result = e.checkCondition(node, expressionEngine);
              assertThat(result).describedAs(node::toString).isTrue();
            });
          } else {
            var result = e.checkCondition(element, expressionEngine);
            assertThat(result).describedAs(() -> element == null ? "" : element.toString())
                .isTrue();
          }
        });
  }

  private void compare(Object actual, boolean isNot, ParameterTypes.Comparison comparison,
                       Object expected) {
    var expectObj = expected;
    var assertObject = switch (comparison) {
      case CONTAIN, EQUAL -> {
        var result = assertThat(actual).extracting(s -> jsonMapper.valueToTree(actual))
            .usingRecursiveComparison()
            .withComparatorForType(Comparator.comparing(NumericNode::decimalValue),
                NumericNode.class).ignoringCollectionOrder().asInstanceOf(JSON);

        if (comparison == ParameterTypes.Comparison.CONTAIN) {
          result = assertJsonConfig.configForContain(result);
        } else {
          result = assertJsonConfig.configForEqual(result);
        }

        expectObj = jsonMapper.valueToTree(expected);
        yield result;
      }
      default -> assertThat(actual);
    };

    if (isNot) {
      assertObject.isNotEqualTo(expectObj);
    } else {
      assertObject.isEqualTo(expectObj);
    }
  }

  private void compare(String actualExpression, boolean isNot, ParameterTypes.Comparison comparison,
                       Object expected) {
    var actual = variableResolvers.resolve(actualExpression, false);
    compare(actual, isNot, comparison, expected);
  }

  @And("{string}( should){not}(be ){comparison}")
  public void compareDocString(String actualExpression, boolean isNot,
                               ParameterTypes.Comparison comparison, String expectedYaml) {
    var expected = yamlExpressionParser.loadExpression(expectedYaml, ExpressionSyntax.TEMPLATE);
    compare(actualExpression, isNot, comparison, expected);
  }

  @And("{string}( should){not}(be ){comparison} {string}")
  public void compareExpression(String actualExpression, boolean isNot,
                                ParameterTypes.Comparison comparison, String expectExpression) {
    var expected = expressionEngine.evaluate(expectExpression, false);
    compare(actualExpression, isNot, comparison, expected);
  }

  @And("{words} should{not}(be ){comparison}")
  public void compareRegisterVariableDocString(String variable, boolean isNot,
                                               ParameterTypes.Comparison comparison,
                                               String expectedYaml) {
    var expected = yamlExpressionParser.loadExpression(expectedYaml, ExpressionSyntax.TEMPLATE);
    var actual = variableResolvers.resolve(variable, true);
    compare(actual, isNot, comparison, expected);
  }

  @And("{words} should{not}(be ){comparison} {string}")
  public void compareRegisterVariable(String variable, boolean isNot,
                                      ParameterTypes.Comparison comparison,
                                      String expectExpression) {
    var expected = expressionEngine.evaluate(expectExpression, false);
    var actual = variableResolvers.resolve(variable, true);
    compare(actual, isNot, comparison, expected);
  }
}
