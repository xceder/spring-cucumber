package com.xceder.cucumber.steps;

import io.cucumber.java.ParameterType;
import java.util.Objects;
import javax.annotation.Nullable;

public class ParameterTypes {
  @ParameterType("at (\\d+) seconds? timeout")
  public int timeoutSeconds(String timeoutVal) {
    return Integer.parseInt(timeoutVal);
  }


  @ParameterType("[^\"]+?[^\\\\b]")
  public String words(String variable) {
    return variable;
  }

  @ParameterType(" |( clone ?)")
  public boolean clone(@Nullable String raw) {
    return !Objects.requireNonNullElse(raw, "").trim().isEmpty();
  }

  @ParameterType(" |( not ?)")
  public boolean not(@Nullable String raw) {
    return !Objects.requireNonNullElse(raw, "").trim().isEmpty();
  }

  @ParameterType("succeed|fail")
  public boolean isSucceed(@Nullable String raw) {
    return Objects.requireNonNullElse(raw, "").equals("succeed");
  }

  @ParameterType("match(?:es)?|contains?|equals? to|same as|be")
  public Comparison comparison(@Nullable String raw) {
    var formatted = Objects.requireNonNullElse(raw, "")
        .trim()
        .split("\\s")[0]
        .trim()
        .toUpperCase()
        .replaceAll("(ES|S)$", "");

    var key = switch (formatted) {
      case "MATCH" -> "CONTAIN";
      case "BE" -> "SAME";
      default -> formatted;
    };

    return Comparison.valueOf(key);
  }

  public enum Comparison {
    CONTAIN,
    EQUAL,
    SAME,
  }
}
