package com.xceder.cucumber.steps;

import com.xceder.cucumber.expression.ExpressionSyntax;
import com.xceder.cucumber.restful.RestRequest;
import com.xceder.cucumber.restful.RestResponse;
import com.xceder.cucumber.yaml.YamlExpressionParser;
import io.cucumber.java.DocStringType;

public class RestRequestConverts {
  private final YamlExpressionParser yamlExpressionParser;

  public RestRequestConverts(YamlExpressionParser yamlExpressionParser) {
    this.yamlExpressionParser = yamlExpressionParser;
  }

  @DocStringType
  public RestRequest restRequest(String yaml) {
    return yamlExpressionParser.loadExpression(yaml,
        ExpressionSyntax.TEMPLATE,
        RestRequest.class);
  }

  @DocStringType
  public RestResponse restResponse(String yaml) {
    return yamlExpressionParser.loadExpression(yaml,
        ExpressionSyntax.TEMPLATE,
        RestResponse.class);
  }
}
