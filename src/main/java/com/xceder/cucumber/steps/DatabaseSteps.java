package com.xceder.cucumber.steps;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.database.DBExecutor;
import com.xceder.cucumber.database.DatabaseExpressionResolver;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.yaml.YamlExpressionParser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import javax.sql.DataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

public class DatabaseSteps {
  private final DBExecutor executor;
  private final DataSource dataSource;
  private final CucumberSpelExpression expressionEngine;
  private final DatabaseExpressionResolver queryResultResolver;
  private final JsonMapper jsonMapper;

  public DatabaseSteps(DBExecutor dbExecutor,
                       DataSource dataSource,
                       CucumberSpelExpression expressionEngine,
                       DatabaseExpressionResolver queryResultResolver,
                       Supplier<JsonMapper> jsonMapperSupplier) {
    this.executor = dbExecutor;
    this.dataSource = dataSource;
    this.expressionEngine = expressionEngine;
    this.queryResultResolver = queryResultResolver;
    this.jsonMapper = jsonMapperSupplier.get();
  }

  @Given("{string} is loaded into the database")
  public void isLoadedIntoTheSystemDatabase(String sqlFilePath) {
    var resourceDatabasePopulator =
        new ResourceDatabasePopulator(false,
            false,
            "UTF-8",
            new ClassPathResource(sqlFilePath));
    resourceDatabasePopulator.execute(dataSource);
  }

  @When("Table {string} has record")
  @And("save table {string} with")
  @Transactional
  public Object saveTable(String entityName, Map<String, ?> entityMap) {
    var entityClass = executor.getEntityClass(entityName);
    var entity = jsonMapper.convertValue(entityMap, entityClass);
    return executor.save(entity);
  }

  @And("save table {string} as {string} with")
  @Transactional
  public void saveTable(String entityName, String label, Map<String, ?> entityMap) {
    assertThat(label).isNotEmpty();

    var entity = saveTable(entityName, entityMap);
    expressionEngine.getRootObject().put(label, entity);
  }

  @When("Table {string} has record {string}")
  @And("save table {string} with {string}")
  @SuppressWarnings("unchecked")
  @Transactional
  public void saveTable(String entityName, String expression) {
    var entityMap = (Map<String, ?>) expressionEngine.evaluate(expression, Map.class);
    saveTable(entityName, entityMap);
  }

  @Then("Table {string} should{not}have record")
  public void tableContains(String entityName, boolean isNot, Map<String, ?> entityMap) {
    assertThat(entityMap)
        .withFailMessage(
            () -> String.format("%s should%s exists in table '%s'",
                entityMap.toString(),
                isNot ? " not" : "",
                entityName))
        .matches((e) -> {
          var isExist = executor.exists(entityName, entityMap);
          return isNot ^ isExist;
        });
  }

  @Then("Table {string} should{not}have record {string}")
  @SuppressWarnings("unchecked")
  public void tableContains(String entityName, boolean isNot, String expression) {
    var entityMap = (Map<String, ?>) expressionEngine.evaluate(expression, Map.class);
    tableContains(entityName, isNot, entityMap);
  }

  @When("query table/Table {string} with")
  public void queryTableWith(String entityName, Map<String, ?> entityMap) {
    queryTableWith(entityName, -1, entityMap);
  }

  @When("query table {string} {timeoutSeconds}")
  public void queryTableWith(String entityName, int timeoutSeconds, Map<String, ?> entityMap) {
    List<?> results;

    if (timeoutSeconds > 0) {
      results = await()
          .alias(String.format("table '%s' query", entityName))
          .atMost(timeoutSeconds, TimeUnit.SECONDS)
          .until(() -> executor.find(entityName, entityMap), r -> !r.isEmpty());

    } else {
      results = executor.find(entityName, entityMap);
    }

    queryResultResolver.setQueryResults(results);
  }

  @SuppressWarnings("unchecked")
  @When("query table {string} with {string} {timeoutSeconds}")
  public void queryTableWith(String entityName, String expression, int timeoutSeconds) {
    var entityMap = (Map<String, ?>) expressionEngine.evaluate(expression, Map.class);
    queryTableWith(entityName, timeoutSeconds, entityMap);
  }

  @When("query table {string} with {string}")
  public void queryTableWith(String entityName, String expression) {
    queryTableWith(entityName, expression, -1);
  }
}
