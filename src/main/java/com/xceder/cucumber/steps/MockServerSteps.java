package com.xceder.cucumber.steps;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockserver.model.HttpRequest.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.mockserver.MockRequestCalls;
import com.xceder.cucumber.mockserver.MockServerFactory;
import com.xceder.cucumber.restful.RestRequest;
import com.xceder.cucumber.restful.RestResponse;
import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import javax.annotation.Nullable;
import org.mockserver.matchers.Times;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class MockServerSteps {
  private final Map<String, List<String>> mockServerExpectationMap = new HashMap<>();
  private final CucumberSpelExpression expressionEngine;
  private final JsonMapper jsonMapper;
  private final MockServerFactory mockServerFactory;

  public MockServerSteps(CucumberSpelExpression expressionEngine,
                         Supplier<JsonMapper> jsonMapperSupplier,
                         MockServerFactory mockServerFactory) {
    this.expressionEngine = expressionEngine;
    this.jsonMapper = jsonMapperSupplier.get();
    this.mockServerFactory = mockServerFactory;
  }

  @DataTableType
  public MockRequestCalls mockRequestCalls(Map<String, String> row) {
    var calls = row.getOrDefault("calls", null);
    return new MockRequestCalls(row.get("request"),
        calls == null || calls.isBlank() ? 0 : Integer.parseInt(calls),
        row.get("response"));
  }

  @Nullable
  private String toBodyString(@Nullable Object body) throws JsonProcessingException {
    return body == null ? null : jsonMapper.writeValueAsString(body);
  }

  private HttpRequest toMockRequest(RestRequest request) throws JsonProcessingException {
    var result = request();
    if (request.getQueryParameters() != null) {
      request.getQueryParameters()
          .forEach((k, v) -> result.withQueryStringParameter(k,
              Objects.requireNonNullElse(v, "").toString()));
    }

    result.withPath(URI.create(request.getUrl()).getPath());

    if (request.getHeaders() != null) {
      request.getHeaders().forEach((k, v) -> v.forEach(vv -> result.withHeader(k, vv)));
    }

    String method = HttpMethod.GET.name();

    var body = toBodyString(request.getBody());
    if (body != null) {
      result.withBody(body);
      method = HttpMethod.POST.name();
    }

    if (request.getMethod() != null) {
      method = request.getMethod().name();
    }

    result.withMethod(method);
    return result;
  }

  private HttpResponse toMockResponse(RestResponse response) throws JsonProcessingException {
    var result = new HttpResponse();

    var statusCode = response.getStatusCode();
    if (statusCode == null) {
      statusCode = HttpStatus.OK;
    }

    result.withStatusCode(statusCode.value());

    if (response.getHeaders() != null) {
      response.getHeaders().forEach((k, v) -> v.forEach(vv -> result.withHeader(k, vv)));
    }

    var body = toBodyString(response.getBody());
    result.withBody(body);

    return result;
  }

  @After
  public void afterScenario() {
    mockServerExpectationMap.forEach((k, v) -> resetMockServer(k));
  }

  @Given("reset mock server {string}")
  public void resetMockServer(String label) {
    var expectationList = mockServerExpectationMap.get(label);

    var server = mockServerFactory.getMockServer(label);
    if (server != null) {
      expectationList.forEach(server::clear);
    }
  }

  @Then("mock server {string} calls verification should {isSucceed}")
  public void mockServerVerificationShouldSucceed(String label, boolean isSucceed) {
    var expectList = mockServerExpectationMap.getOrDefault(label, Collections.emptyList());
    var mockServer = Objects.requireNonNull(mockServerFactory.getMockServer(label));

    var exceptionExpect = catchThrowableOfType(AssertionError.class, () -> {
      if (expectList.isEmpty()) {
        throw new AssertionError("mock server isn't configured");
      } else {
        mockServer.verify(expectList.toArray(new String[0]));
      }
    });

    var assertThat =
        assertThat(exceptionExpect)
            .describedAs("mock server " + label + " calls verification");

    if (isSucceed) {
      assertThat.doesNotThrowAnyException();
    } else {
      assertThat.isNotNull();
    }
  }

  @When("mock server {string} calls are configured")
  public void mockServerCallsAreConfigured(String label, List<MockRequestCalls> expectedCalls)
      throws JsonProcessingException {
    for (var mockRequestCall : expectedCalls) {

      var mockServer = mockServerFactory.getMockServer(label);
      assert mockServer != null;

      var request = expressionEngine.evaluate(mockRequestCall.request(), RestRequest.class);
      var response = expressionEngine.evaluate(mockRequestCall.response(), RestResponse.class);

      Times times;

      if (mockRequestCall.calls() >= 0) {
        times = Times.exactly(mockRequestCall.calls());
      } else {
        times = Times.unlimited();
      }

      var expectations = mockServer.when(toMockRequest(request), times)
          .respond(toMockResponse(response));

      for (var expectation : expectations) {
        mockServerExpectationMap.computeIfAbsent(label, l -> new ArrayList<>())
            .add(expectation.getId());
      }
    }
  }

  @And("^start mock server \"(.+)\"(?: at port (\\d+))?")
  public void startMockServerAtPort(String label, @Nullable Integer portParam) throws IOException {
    var server = mockServerFactory.startMockServer(label, portParam == null ? 0 : portParam);
    expressionEngine.getRootObject().put(label, server);
  }
}
