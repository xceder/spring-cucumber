package com.xceder.cucumber;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfoList;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import lombok.SneakyThrows;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;

@ThreadSafe
public class SpringBeanUtils {
  private final ApplicationContext context;
  private final List<String> scanPackages;
  private final Map<String, Class<?>> classInfoCache = new HashMap<>();
  private final List<ClassInfoList> packageClassInfoList = new ArrayList<>();

  public SpringBeanUtils(ApplicationContext context, List<String> scanPackages) {
    this.context = context;
    this.scanPackages = scanPackages;
  }

  public void addScanPackage(String packageName) {
    scanPackages.add(packageName);
  }

  public Properties parseProps(String yamlProps) {
    YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
    ByteArrayResource resource = new ByteArrayResource(yamlProps.getBytes(StandardCharsets.UTF_8));
    yamlPropertiesFactoryBean.setResources(resource);
    yamlPropertiesFactoryBean.afterPropertiesSet();
    return yamlPropertiesFactoryBean.getObject();
  }

  public <T> T createBeanWith(Properties properties, Class<T> beanType) {
    System.getProperties().putAll(properties);
    var beanFactory = context.getAutowireCapableBeanFactory();
    return beanFactory.createBean(beanType);
  }

  @SneakyThrows
  @Nullable
  public Class<?> findClassByNameSuffix(String suffix) {
    synchronized (classInfoCache) {
      if (packageClassInfoList.isEmpty()) {
        Set<String> processedPackages = new HashSet<>();
        for (var packageName : scanPackages) {
          if (!processedPackages.contains(packageName)) {
            try (var scanResult = new ClassGraph().enableSystemJarsAndModules()
                .acceptPackages(packageName).scan()) {
              var classInfoList = scanResult.getSubclasses(Object.class.getName());
              packageClassInfoList.add(classInfoList);
              processedPackages.add(packageName);
            }
          }
        }
      }
    }

    Class<?> result = classInfoCache.get(suffix);

    if (result == null) {

      String className = findClass(suffix);

      if (className != null) {
        result = Class.forName(className);
        classInfoCache.put(suffix, result);
      }
    }

    return result;
  }

  private @Nullable String findClass(String suffix) {
    String className = null;

    List<String> matchedSuffixList = new ArrayList<>();

    outerLoop:
    for (var classInfoList : packageClassInfoList) {
      for (var classInfo : classInfoList) {
        if (classInfo.getSimpleName().equals(suffix)) {
          className = classInfo.getName();
          break outerLoop;
        } else if (classInfo.getName().endsWith(suffix)) {
          matchedSuffixList.add(classInfo.getName());
        }
      }
    }

    if (className == null && !matchedSuffixList.isEmpty()) {
      matchedSuffixList.sort(String.CASE_INSENSITIVE_ORDER);
      className = matchedSuffixList.iterator().next();
    }
    return className;
  }
}
