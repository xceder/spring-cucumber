package com.xceder.cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.xceder.cucumber.database.DBExecutor;
import com.xceder.cucumber.database.DatabaseExpressionResolver;
import com.xceder.cucumber.database.EntityClassExecutor;
import com.xceder.cucumber.expression.CucumberSpelExpression;
import com.xceder.cucumber.expression.ExpressionResolverRegister;
import com.xceder.cucumber.expression.ExpressionRoot;
import com.xceder.cucumber.expression.ScenarioCounter;
import com.xceder.cucumber.expression.VariableResolvers;
import com.xceder.cucumber.json.HttpMethodDeserializer;
import com.xceder.cucumber.json.HttpMethodSerializer;
import com.xceder.cucumber.mockserver.MockServerFactory;
import com.xceder.cucumber.restful.RestRequestResponseRecords;
import com.xceder.cucumber.yaml.AssertThatJsonConfig;
import com.xceder.cucumber.yaml.ClassResolver;
import com.xceder.cucumber.yaml.YamlExpressionParser;
import com.xceder.cucumber.yaml.YamlTagClassResolver;
import com.xceder.cucumber.yaml.types.InstantTypeDescription;
import io.cucumber.spring.ScenarioScope;
import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.function.Supplier;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.client.RestTemplateBuilderConfigurer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.web.ProjectingJackson2HttpMessageConverter;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.util.function.SingletonSupplier;
import org.yaml.snakeyaml.TypeDescription;

@Lazy
@Configuration
@ComponentScan(basePackageClasses = InstantTypeDescription.class)
public class CucumberBeansConfig {
  @Bean
  @ConditionalOnMissingBean(RestTemplateBuilder.class)
  @ConditionalOnClass({RestTemplateBuilderConfigurer.class, RestTemplateBuilder.class})
  public RestTemplateBuilder restTemplateBuilder(
      RestTemplateBuilderConfigurer restTemplateBuilderConfigurer, ObjectMapper objectMapper) {
    RestTemplateBuilder builder = new RestTemplateBuilder();
    return restTemplateBuilderConfigurer.configure(builder)
        .additionalMessageConverters(new ProjectingJackson2HttpMessageConverter(objectMapper));
  }

  @Bean
  @ConditionalOnMissingBean
  Supplier<JsonMapper> jsonMapperSuppler(Jackson2ObjectMapperBuilder builder) {
    var jsonMapper = new JsonMapper();
    builder.configure(jsonMapper);
    var module = new SimpleModule()
        .addDeserializer(HttpMethod.class, new HttpMethodDeserializer())
        .addSerializer(HttpMethod.class, new HttpMethodSerializer());

    jsonMapper.registerModule(module).registerModule(new JavaTimeModule());
    return SingletonSupplier.of(jsonMapper);
  }

  @Bean
  public SpringBeanUtils springBeanUtils(ApplicationContext context) {
    var packageList = new ArrayList<String>();
    packageList.add(CucumberBeansConfig.class.getPackage().getName());
    packageList.add("java.lang");
    packageList.add("java.util");
    packageList.add("java.sql");
    packageList.add("java");

    var autoPackage = AutoConfigurationPackages.get(context.getAutowireCapableBeanFactory());
    packageList.addAll(autoPackage);

    return new SpringBeanUtils(context, packageList);
  }

  @Bean
  public YamlTagClassResolver yamlTagClassResolver(SpringBeanUtils springBeanUtils) {
    return springBeanUtils::findClassByNameSuffix;
  }

  @Bean
  public ClassResolver classSimpleNameResolver(SpringBeanUtils springBeanUtils) {
    return springBeanUtils::findClassByNameSuffix;
  }

  @Bean
  @ConditionalOnMissingBean(DBExecutor.class)
  @ConditionalOnClass(EntityManager.class)
  public EntityClassExecutor entityClassExecutor(ClassResolver resolver) {
    return new EntityClassExecutor(resolver);
  }

  @Bean
  @ConditionalOnMissingBean(AssertThatJsonConfig.class)
  public AssertThatJsonConfig assertThatJsonConfig() {
    return new AssertThatJsonConfig() {
    };
  }

  @Bean
  @ScenarioScope
  @ConditionalOnMissingBean(ExpressionRoot.class)
  public ExpressionRoot expressionRoot(Supplier<JsonMapper> jsonMapperSupplier,
                                       ScenarioCounter scenarioCounter) {
    return new ExpressionRoot(jsonMapperSupplier, scenarioCounter);
  }

  @Bean
  @ScenarioScope
  public CucumberSpelExpression cucumberSpelExpression(ExpressionRoot expressionRoot) {
    return new CucumberSpelExpression(expressionRoot);
  }

  @Lazy(false)
  @Bean
  @ScenarioScope(proxyMode = ScopedProxyMode.NO)
  public VariableResolvers variableResolvers(
      CucumberSpelExpression expressionEngine,
      ObjectProvider<ExpressionResolverRegister<?>> expressionResolverRegisters) {
    var variableResolvers = new VariableResolvers(expressionEngine);
    expressionResolverRegisters.forEach(r ->
        r.getSupportedExpressions().forEach(e -> variableResolvers.register(e, r)));

    return variableResolvers;
  }

  @Bean
  @ScenarioScope
  public DatabaseExpressionResolver databaseExpressionResolver() {
    return new DatabaseExpressionResolver();
  }

  @Bean
  @ScenarioScope
  public ScenarioCounter scenarioCounter() {
    return new ScenarioCounter();
  }

  @Bean
  @ConditionalOnClass(ClientAndServer.class)
  public MockServerFactory mockServerFactory() {
    return new MockServerFactory();
  }

  @Bean
  @ScenarioScope
  public RestRequestResponseRecords restRequestResponseRecords() {
    return new RestRequestResponseRecords();
  }

  @Bean
  @ScenarioScope
  public YamlExpressionParser yamlExpressionParser(
      Supplier<JsonMapper> jsonMapperSupplier,
      YamlTagClassResolver yamlTagClassResolver,
      CucumberSpelExpression expressionEngine,
      ObjectProvider<TypeDescription> typeDescriptionProvider) {
    return new YamlExpressionParser(jsonMapperSupplier, yamlTagClassResolver, expressionEngine,
        typeDescriptionProvider);
  }
}
